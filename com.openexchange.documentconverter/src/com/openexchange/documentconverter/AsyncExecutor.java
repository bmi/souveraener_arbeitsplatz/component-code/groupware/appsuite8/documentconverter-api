/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * {@link AsyncConverter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.0
 */
/**
 * {@link AsyncExecutor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.0
 */
public class AsyncExecutor implements ICounter {

    final protected static int MAX_REQUEST_QEUE_SIZE = 16384;

    final protected static long CLEANUP_TIMEOUT_MILLIS = 600000;

    /**
     * {@link AsyncIdentifier}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.8.0
     */
    protected static class AsyncIdentifier {

        protected AsyncIdentifier(@NonNull final String asyncHash) {
            super();

            m_asyncHash = asyncHash;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            return (31 + ((m_asyncHash == null) ? 0 : m_asyncHash.hashCode()));
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }

            if ((obj == null) || getClass() != obj.getClass()) {
                return false;
            }

            AsyncIdentifier other = (AsyncIdentifier) obj;

            if (m_asyncHash == null) {
                if (other.m_asyncHash != null) {
                    return false;
                }
            } else if (!m_asyncHash.equals(other.m_asyncHash)) {
                return false;
            }

            return true;
        }

        // - API ---------------------------------------------------------------

        /**
         * @return
         */
        protected String getAsyncHash() {
            return m_asyncHash;
        }

        /**
         * @return
         */
        protected long getTimestampMillis() {
            return m_timestampMillis;
        }

        // - Members -----------------------------------------------------------

        final private String m_asyncHash;

        final private long m_timestampMillis = System.currentTimeMillis();
    }

    /**
     * {@link AsyncRunnable}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.8.0
     */
    protected class AsyncRunnable implements Runnable {

        /**
         * Initializes a new {@link AsyncRunnable}.
         * @param jobType
         * @param jobProperties
         */
        protected AsyncRunnable(@NonNull String jobType, @NonNull HashMap<String, Object> jobProperties) {
            m_jobType = jobType;
            m_jobProperties = new HashMap<>(jobProperties.size());
            m_jobProperties.putAll(jobProperties);

            final File inputFile = (File) jobProperties.get(Properties.PROP_INPUT_FILE);

            try (final InputStream inputStm = (InputStream) jobProperties.get(Properties.PROP_INPUT_STREAM)) {
                if (StringUtils.isNotEmpty(m_jobType) && ((null != inputFile) || (null != inputStm))) {
                    final File tempInputFile = m_manager.createTempFile("oxasync");

                    if (null != tempInputFile) {
                        if (null != inputFile) {
                            FileUtils.copyFile(inputFile, tempInputFile);
                            m_jobProperties.put(Properties.PROP_INPUT_FILE, tempInputFile);
                            m_asyncId = implCalculateAsyncHash(null);
                        } else if (null != inputStm) {
                            FileUtils.copyInputStreamToFile(inputStm, tempInputFile);

                            // replace the original input content file temp. input file
                            m_jobProperties.remove(Properties.PROP_INPUT_STREAM);
                            m_jobProperties.put(Properties.PROP_INPUT_FILE, tempInputFile);
                            m_asyncId = implCalculateAsyncHash(null);
                        }
                    }
                }
            } catch (final Exception e) {
                DocumentConverterManager.logExcp(e);
            }

            if (!isValid()) {
                // valid input as well, if a remote cache hash input source is set
                final String remoteCacheHash = (String) jobProperties.get(Properties.PROP_REMOTE_CACHE_HASH);

                if (StringUtils.isNotEmpty(remoteCacheHash)) {
                    m_asyncId = implCalculateAsyncHash(remoteCacheHash);
                }
            }

            if (isValid()) {
                // ensure, that we don't run into an async recursion,
                // but also ensure, that the remote side still treats
                // our request as async request
                m_jobProperties.remove(Properties.PROP_ASYNC);
                m_jobProperties.put(Properties.PROP_REMOTE_ASYNC, Boolean.TRUE);

                final JobPriority jobPriority = (JobPriority) m_jobProperties.get(Properties.PROP_PRIORITY);

                if (null == jobPriority) {
                    m_jobProperties.put(Properties.PROP_PRIORITY, JobPriority.BACKGROUND);
                }
            } else {
                clear();
            }
        }

        /* (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            // reduce parent queue count by one
            decrement();

            if (isValid()) {
                logDebug("DC started asynchronous conversion", m_jobProperties);

                final HashMap<String, Object> resultProperties = new HashMap<>(8);

                DocumentConverterManager.close(m_documentConverter.convert(m_jobType, m_jobProperties, resultProperties));

                // increment run counter by one
                m_ranCounter.incrementAndGet();

                logDebug("DC finished asynchronous conversion", m_jobProperties);
            }

            clear();
        }

        /**
         * @return
         */
        protected HashMap<String, Object> getJobProperties() {
            return m_jobProperties;
        }

        /**
         * @return
         */
        protected AsyncIdentifier getAsyncIdentifier() {
            return m_asyncId;
        }

        /**
         * @return
         */
        protected synchronized boolean isValid() {
            return (null != m_asyncId);
        }

        /**
         *
         */
        protected synchronized void clear() {
            if (null != m_jobProperties) {
                FileUtils.deleteQuietly((File) m_jobProperties.remove(Properties.PROP_INPUT_FILE));
            }

            m_jobType = null;
            m_jobProperties = null;
            m_asyncId = null;
        }

        /**
         * @param remoteCashHash
         * @return
         */
        private AsyncIdentifier implCalculateAsyncHash(final String remoteCashHash) {
            StringBuilder asyncHashBuilder = new StringBuilder();
            AsyncIdentifier ret = null;

            if (StringUtils.isNotEmpty(remoteCashHash)) {
                asyncHashBuilder.append(remoteCashHash).append('-');
            } else {
                final File inputFile = (File) m_jobProperties.get(Properties.PROP_INPUT_FILE);
                final String inputURL = (String) m_jobProperties.get(Properties.PROP_INPUT_URL);
                final String locale = (String) m_jobProperties.get(Properties.PROP_LOCALE);
                final StringBuilder fileHashBuilder = DocumentConverterManager.getFileHashBuilder(inputFile, inputURL, locale);

                if (null != fileHashBuilder) {
                    asyncHashBuilder.append(m_jobType).append('-').append(fileHashBuilder.toString());
                } else {
                    asyncHashBuilder = null;
                }
            }

            if (null != asyncHashBuilder) {
                asyncHashBuilder.append(((JobPriority) m_jobProperties.get(Properties.PROP_PRIORITY)).toString());
                ret = new AsyncIdentifier(asyncHashBuilder.toString());
            }

            return ret;
        }

        // - Members -------------------------------------------------------

        protected String m_jobType = null;
        protected HashMap<String, Object> m_jobProperties = null;
        protected AsyncIdentifier m_asyncId = null;
    }

    /**
     * Initializes a new {@link AsyncExecutor}.
     * @param manager
     * @param maxThreadCount
     */
    public AsyncExecutor(@NonNull IDocumentConverter converter, @NonNull DocumentConverterManager manager, int maxThreadCount, int queueSize) {
        super();

        m_documentConverter = converter;
        m_manager = manager;

        m_requestQueue = new ArrayBlockingQueue<>(queueSize);

        if (maxThreadCount > 0) {
            m_requestExecutor = new ThreadPoolExecutor(maxThreadCount, maxThreadCount, 0, TimeUnit.MILLISECONDS, m_requestQueue);

            // ensure, that entry is cleaned up (e.g. remove tmp. files) if execution is rejected
            m_requestExecutor.setRejectedExecutionHandler((runnable, executor) -> {
                if (null != runnable) {
                    m_droppedCounter.incrementAndGet();
                    decrement();
                    ((AsyncRunnable) runnable).clear();
                }
            });

            // initialize timer to clean up run set
            m_timerExecutor.scheduleAtFixedRate(() -> {
                final long curTimeMillis = System.currentTimeMillis();

                synchronized (m_runSet) {
                    for (final Iterator<AsyncIdentifier> asyncIdIter = m_runSet.iterator(); asyncIdIter.hasNext();) {
                        if ((curTimeMillis - asyncIdIter.next().getTimestampMillis()) > CLEANUP_TIMEOUT_MILLIS) {
                            asyncIdIter.remove();
                        } else {
                            // since the items are ordered by time, we
                            // can stop the search when the first item
                            // did not reach its timeout
                            break;
                        }
                    }
                }
            }, CLEANUP_TIMEOUT_MILLIS, CLEANUP_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        }
    }

    // - ICounter --------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.ICounter#increment()
     */
    @Override
    public void increment() {
        // ok
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.ICounter#decrement()
     */
    @Override
    public void decrement() {
        // ok
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.ICounter#getCount()
     */
    @Override
    public int getScheduledCount() {
        return m_requestQueue.size();
    }

    // - public API ------------------------------------------------------------

    public void shutdown() {
        if (m_isRunning.compareAndSet(true, false)) {
            final boolean trace = DocumentConverterManager.isLogTrace();
            long traceStartTimeMillis = 0;

            AsyncExecutor.AsyncRunnable curRunnable = null;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                DocumentConverterManager.logTrace("DC AsyncExecutor starting shutdown...");
            }

            while((curRunnable = (AsyncExecutor.AsyncRunnable) m_requestQueue.poll()) != null) {
                // decrement queue count by one
                decrement();
                curRunnable.clear();
            }

            if (trace) {
                DocumentConverterManager.logTrace(new StringBuilder(256).
                    append("DC AsyncExecutor finished shutdown: ").
                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").
                    append('!').toString());
            }
        }
    }

    /**
     * @return
     */
    boolean isRunning() {
        return m_isRunning.get();
    }

    /**
     * @param jobType
     * @param jobProperties
     * @param resultProperties
     */
    public void triggerExecution(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        if (isRunning() && (null != m_requestExecutor) && (null != jobType) && (null != jobProperties) && (null != resultProperties)) {
            final AsyncExecutor.AsyncRunnable asyncRunnable = new AsyncExecutor.AsyncRunnable(jobType, jobProperties);

            if (asyncRunnable.isValid()) {
                if (needsRunning(asyncRunnable)) {
                    logDebug("DC scheduled asynchronous job", asyncRunnable.getJobProperties());

                    // increment queue count by one
                    increment();
                    m_requestExecutor.execute(asyncRunnable);

                } else {
                    logDebug("DC already scheduled same asynchronous job => omitting request", asyncRunnable.getJobProperties());

                    m_omitCounter.incrementAndGet();
                    asyncRunnable.clear();
                }
            } else {
                if (DocumentConverterManager.isLogWarn()) {
                    DocumentConverterManager.logWarn("DC is not able to execute async. job without file input, stream input or cached hash value set => please set source input property accordingly");
                }
            }
        }
    }

    /**
     * @return
     */
    public long getProcessedCount() {
        return m_ranCounter.get();
    }

    /**
     * @return
     */
    public long getDroppedCount() {
        return m_droppedCounter.get();
    }

    /**
     * @return
     */
    public LogData[] getLogData() {
        return new LogData[] {
            new LogData("async_jobs_remaining", Long.toString(getScheduledCount())),
            new LogData("async_jobs_ran", m_ranCounter.toString()),
            new LogData("async_job_omitted", m_omitCounter.toString())
        };
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param asyncRunnable
     * @return
     */
    protected boolean needsRunning(@NonNull final AsyncRunnable asyncRunnable) {
        final AsyncIdentifier asyncId = asyncRunnable.getAsyncIdentifier();
        boolean ret = true;

        synchronized (m_runSet) {
            // if entry with same AsyncId is already contained
            // in run set => remove old entry and put new entry
            // with up to date timestamp at end of time based
            // ordered LinkedHashSet
            if (m_runSet.remove(asyncId)) {
                ret = false;
            }

            // put entry at end of the set in every case
            m_runSet.add(asyncId);
        }


        return ret;
    }

    /**
     * @param text
     * @param jobProperties
     */
    protected void logDebug(@NonNull final String text, @NonNull final HashMap<String, Object> jobProperties) {
        if (DocumentConverterManager.isLogDebug()) {
            DocumentConverterManager.logDebug(text, jobProperties, getLogData());
        }
    }

    // - Members -----------------------------------------------------------

    final protected IDocumentConverter m_documentConverter;
    final protected DocumentConverterManager m_manager;
    final protected BlockingQueue<Runnable> m_requestQueue;
    final protected LinkedHashSet<AsyncIdentifier> m_runSet = new LinkedHashSet<>();
    final protected ScheduledExecutorService m_timerExecutor = Executors.newScheduledThreadPool(1);
    final protected AtomicBoolean m_isRunning = new AtomicBoolean(true);
    final protected AtomicLong m_ranCounter = new AtomicLong(0);
    final protected AtomicLong m_omitCounter = new AtomicLong(0);
    final protected AtomicLong m_droppedCounter = new AtomicLong(0);
    protected ThreadPoolExecutor m_requestExecutor = null;
}
