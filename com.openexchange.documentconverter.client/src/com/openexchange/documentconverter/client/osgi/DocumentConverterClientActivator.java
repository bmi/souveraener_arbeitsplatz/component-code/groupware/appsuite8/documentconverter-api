/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.osgi;

import org.apache.commons.io.FileUtils;
import org.eclipse.microprofile.health.HealthCheck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory;
import com.openexchange.ajax.requesthandler.osgiservice.AJAXModuleActivator;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Reloadable;
import com.openexchange.documentconverter.FileIdManager;
import com.openexchange.documentconverter.HttpHelper;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.client.impl.ClientConfig;
import com.openexchange.documentconverter.client.impl.ClientManager;
import com.openexchange.documentconverter.client.impl.DCHealthCheck;
import com.openexchange.documentconverter.client.json.DocumentConverterActionFactory;
import com.openexchange.documentconverter.client.preview.DocumentConverterPreviewService;
import com.openexchange.file.storage.composition.IDBasedFileAccessFactory;
import com.openexchange.file.storage.composition.IDBasedFolderAccessFactory;
import com.openexchange.file.storage.composition.crypto.CryptographicAwareIDBasedFileAccessFactory;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.imagetransformation.ImageTransformationService;
import com.openexchange.mail.compose.CompositionSpaceServiceFactoryRegistry;
import com.openexchange.net.ssl.SSLSocketFactoryProvider;
import com.openexchange.preview.InternalPreviewService;

public class DocumentConverterClientActivator extends AJAXModuleActivator {

    /**
     * Initializes a new {@link DocumentConverterClientActivator}.
     */
    public DocumentConverterClientActivator() {
        super();
    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { ConfigurationService.class,
            CapabilityService.class,
            IDBasedFolderAccessFactory.class,
            IDBasedFileAccessFactory.class,
            SSLSocketFactoryProvider.class
        };
    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.HousekeepingActivator#getOptionalServices()
     */
    @Override
    protected Class<?>[] getOptionalServices() {
        return new Class<?>[] {
            IDocumentConverter.class,
            ImageTransformationService.class,
            CryptographicAwareIDBasedFileAccessFactory.class,
            CryptographicServiceAuthenticationFactory.class,
            GuardApi.class
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        if (LOG.isInfoEnabled()) {
            LOG.info("starting bundle: " + SERVICE_NAME);
        }

        final ConfigurationService configService = getService(ConfigurationService.class);

        if (null != configService) {
            final ClientConfig clientConfig = new ClientConfig(configService);

            // initialize HttpHelper singleton with SSL socket factory provider
            HttpHelper.newInstance(getService(SSLSocketFactoryProvider.class));

            // initialize ClientManager singleton
            final ClientManager clientManager = ClientManager.newInstance(clientConfig);

            // initialize FileIdDataManager singleton
            final FileIdManager fileIdManager = FileIdManager.newInstance(clientManager, clientConfig.AUTO_CLEANUP_TIMEOUT_MILLIS);

            LOG.info("Registering DocumentConverter services");
            registerService(IDocumentConverter.class, clientManager, null);
            registerService(Reloadable.class, clientManager);
            registerService(HealthCheck.class, new DCHealthCheck(clientManager));

            // register OfficePreviewService
            LOG.info("Registering DocumentConverterPreviewService");
            registerService(InternalPreviewService.class, new DocumentConverterPreviewService(this));

            // register DocumentConverter related Ajax handlers
            registerModule(new DocumentConverterActionFactory(this, clientManager, fileIdManager, clientConfig), "oxodocumentconverter");

            // register information bean if/when ManagementService is available
            trackService(CompositionSpaceServiceFactoryRegistry.class);
            openTrackers();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void stopBundle() throws Exception {
        // shutdown FileIdDataManager singleton
        final FileIdManager fileIdManager = FileIdManager.get();

        if (null != fileIdManager) {
            fileIdManager.shutdown();
        }

        FileUtils.deleteQuietly(ClientManager.get().getClientConfig().PDFEXTRACTOR_WORKDIR);
        super.stopBundle();
    }

    // - Static Members --------------------------------------------------------

    final private static Logger LOG = LoggerFactory.getLogger(ClientManager.class);

    final private static String SERVICE_NAME = "com.openexchange.documentconverter.client";

    // - Members ---------------------------------------------------------------

}
