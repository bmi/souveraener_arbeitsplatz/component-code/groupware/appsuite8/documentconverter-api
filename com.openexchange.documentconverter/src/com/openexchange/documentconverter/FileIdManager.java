/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.io.FileUtils;

/**
 * {@link FileIdManager}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.4
 */
public class FileIdManager {

    // TODO (KA): set to 5 minutes default for release
    public static final long AUTOREMOVE_TIMEOUT_MILLIS_DEFAULT = 5 * 60 * 1000L;

    /**
     * {@link FileIdData}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.2
     */
    private class FileIdData extends ReentrantLock implements Closeable {

        /**
         * serialVersionUID
         */
        private static final long serialVersionUID = 7239452470741459516L;

        /**
         * Unused
         */
        @SuppressWarnings("unused")
        private FileIdData() {
            // Unused
            super(true);
            m_fileId = "";
        }

        /**
         * Initializes a new {@link FileIdData}.
         */
        FileIdData(@NonNull final String fileId) {
            super(true);

            m_fileId = fileId;
            updateTimestampMillis();
        }

        /**
         * <b>[!!! Unsynchronized !!!]</b> Cleanup of all internally held resources
         */
        @Override
        public void close() {
            implClearData();

            if (DocumentConverterManager.isLogTrace()) {
                DocumentConverterManager.logTrace("DC client FileIdManager.FileIdData closed: " + m_fileId);
            }
        }

        /**
         * @return
         */
        public String getFileId() {
            return m_fileId;
        }

        /**
         * @return
         */
        public long getTimeoutMillis() {
            return m_timeoutMillis.get();
        }

        /**
         * Updates the timeout value, if greater than the already set one
         *
         * @return
         */
        public void setTimeoutMillis(final long newTimeoutMillis) {
            if (newTimeoutMillis > m_timeoutMillis.get()) {
                m_timeoutMillis.set(newTimeoutMillis);
            }
        }

        /**
         * @return
         */
        public long getTimestampMillis() {
            return m_timestampMillis.get();
        }

        /**
         * Updating of timestamp
         */
        public void updateTimestampMillis() {
            m_timestampMillis.set(System.currentTimeMillis());
        }

        /**
         * <b>[!!! Unsynchronized !!!]</b> Getting the temporary file containing the data.
         *
         * @return The temporary file that holds the data.
         *         The file is only valid until this {@link Closeable} object
         *         is finally closed.
         */
        public FileAndURL getDataFileAndURL() {
            return m_dataFileAndURL;
        }

        /**
         * <b>[!!! Unsynchronized !!!]</b> Setting the data content to be stored as temporary file.
         *
         * @param dataInputStream The data to be stored as temporary file.
         * @throws IOException
         */
        public FileAndURL setDataInputStreamAndURL(@Nullable final InputStream dataInputStream, @Nullable String dataURL) throws IOException {
            implClearData();

            if (null != dataInputStream) {
                File dataFile = null;

                if (null != (dataFile = m_manager.createTempFile("oxdcc"))) {
                    FileUtils.copyInputStreamToFile(dataInputStream, dataFile);
                    m_dataFileAndURL = new FileAndURL(dataFile, dataURL);
                } else {
                    throw new IOException("DC Client could not create temp. file");
                }
            }

            return m_dataFileAndURL;
        }

        // - Implementation --------------------------------------------------------

        /**
         *
         */
        private void implClearData() {
            if (null != m_dataFileAndURL) {
                FileUtils.deleteQuietly(m_dataFileAndURL.getFile());
                m_dataFileAndURL = null;
            }
        }

        // - Members ---------------------------------------------------------------

        final private AtomicLong m_timestampMillis = new AtomicLong();

        final private AtomicLong m_timeoutMillis = new AtomicLong(AUTOREMOVE_TIMEOUT_MILLIS_DEFAULT);

        final private String m_fileId;

        private FileAndURL m_dataFileAndURL = null;
    }

    /**
     * Unused
     *
     * Initializes a new {@link FileIdManager}.
     */
    private FileIdManager() {
        // Unused
        m_manager = null;
        m_fileIdDataTimeoutMillis = -1;
    }

    /**
     * Initializes a new {@link FileIdManager}.
     */
    private FileIdManager(@NonNull final DocumentConverterManager manager, final long fileIdTimeoutMillis) {
        m_manager = manager;
        m_fileIdDataTimeoutMillis = fileIdTimeoutMillis;

        // TODO (KA): ensure periodic timer period of 1 minute
        final long autoCleanupPeriodMillis = 1 * 60 * 1000;

        m_autoCleanupCheckTimer.scheduleAtFixedRate(new TimerTask() {

            /*
             * (non-Javadoc)
             *
             * @see java.util.TimerTask#run()
             */
            @Override
            public void run() {
                // in case we're not running anymore,
                // the shutdown method will take care
                // of cleanup of all still open FileIdData
                if (m_isRunning.get()) {
                    implCheckAutoCleanup();
                }
            }
        }, autoCleanupPeriodMillis, autoCleanupPeriodMillis);
    }

    /**
     * @param m_fileManagement
     * @return
     */
    public @NonNull synchronized static FileIdManager newInstance(@NonNull final DocumentConverterManager manager, final long fileIdTimeoutMillis) {
        if (null == fileIdManager) {
            fileIdManager = new FileIdManager(manager, fileIdTimeoutMillis);
        }

        return fileIdManager;

    }

    /**
     * @return
     */
    public @Nullable synchronized static FileIdManager get() {
        return fileIdManager;
    }

    public void shutdown() {
        if (m_isRunning.compareAndSet(true, false)) {
            final boolean trace = DocumentConverterManager.isLogTrace();
            long traceStartTimeMillis = 0;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                DocumentConverterManager.logTrace("DC client FileIdManager starting shutdown...");
            }

            m_autoCleanupCheckTimer.cancel();

            int deletedCount = 0;

            synchronized (m_fileIdMap) {
                for (final String curKey : m_fileIdMap.keySet()) {
                    final FileIdData curFileIdData = m_fileIdMap.get(curKey);

                    if (null != curFileIdData) {
                        // schedule delete of all still open FileIdData objects
                        delete(curFileIdData.getFileId());
                        ++deletedCount;
                    }
                }
            }

            try {
                m_deleteExecutorService.shutdown();
                m_deleteExecutorService.awaitTermination(60, TimeUnit.SECONDS);
            } catch (@SuppressWarnings("unused") InterruptedException e) {
                // ok
            }

            if (trace) {
                DocumentConverterManager.logTrace(new StringBuilder(128).
                    append("DC client FileIdManager auto cleanup scheduled ").append(deletedCount).
                    append(" entries to delete in shudown.").
                    append(" Entries not deleted: ").append(m_fileIdMap.size()).toString());

                DocumentConverterManager.logTrace(new StringBuilder(256).
                    append("DC client FileIdManager finished shutdown: ").
                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").
                    append('!').toString());
            }
        }
    }

    /**
     * @param fileId
     */
    public void lock(final String fileId) {
        if (null != fileId) {
            implLock(fileId, AUTOREMOVE_TIMEOUT_MILLIS_DEFAULT);
        }
    }

    /**
     * @param fileId
     */
    public void lock(final String fileId, final long newTimeoutMillis) {
        if (null != fileId) {
            implLock(fileId, newTimeoutMillis);
        }
    }

    /**
     * @param fileId
     */
    public void unlock(final String fileId) {
        if (null != fileId) {
            implUnlock(fileId);
        }
    }

    /**
     * @param fileId
     * @param dataInputStream
     * @return
     * @throws IOException
     */
    public @Nullable FileAndURL setDataInputStreamAndURL(final String fileId, final InputStream dataInputStream, final String dataURL) throws IOException {
        FileAndURL ret = null;

        if ((null != fileId) && (null != dataInputStream)) {
            // no try-with-resource since we don't want to close FileIdData now;
            // resource is finally closed either by direct call to #delete, by auto cleanup or during shutdown!
            final FileIdData fileIdData = implLock(fileId, AUTOREMOVE_TIMEOUT_MILLIS_DEFAULT);

            try {
                fileIdData.updateTimestampMillis();
                ret = fileIdData.setDataInputStreamAndURL(dataInputStream, dataURL);
            } finally {
                implUnlock(fileId);
            }
        }

        return ret;
    }

    /**
     * @param fileId
     * @return
     */
    public @Nullable FileAndURL getDataFileAndURL(final String fileId) {
        FileAndURL ret = null;

        if (null != fileId) {
            // no try-with-resource since we don't want to close FileIdData now;
            // resource is finally closed either by direct call to #delete, by auto cleanup or during shutdown!
            final FileIdData fileIdData = implLock(fileId, AUTOREMOVE_TIMEOUT_MILLIS_DEFAULT);

            try {
                ret = fileIdData.getDataFileAndURL();
                fileIdData.updateTimestampMillis();
            } finally {
                implUnlock(fileId);
            }
        }

        return ret;
    }

    /**
     * @param fileId
     */
    public void delete(final String fileId) {
        // schedule for asynchronous cleanup (close)
        m_deleteExecutorService.execute(() -> {
            FileIdData fileIdDataToUnlock = null;

            // !!! Closing and resource removal of FileIdData is done by using try-with-resource call in this case !!!
            //
            // lock FileIdData in run method and close it to cleanup
            // entry (temp. file) and to remove from managed map of FileIdData;
            // save current FileIdData in temp. variable to be able to
            // finally unlock the entry without having to use the
            // already reduced map
            try (final FileIdData fileIdData = implLock(fileId, AUTOREMOVE_TIMEOUT_MILLIS_DEFAULT)) {
                fileIdDataToUnlock = fileIdData;

                synchronized (m_fileIdMap) {
                    m_fileIdMap.remove(fileId);
                }

                if (DocumentConverterManager.isLogTrace()) {
                    DocumentConverterManager.logTrace("DC client FileIdManager deleted and removed FileIdData: " + fileId);
                }
            } finally {
                if (null != fileIdDataToUnlock) {
                    fileIdDataToUnlock.unlock();
                }
            }
        });
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param fileId
     * @param newTimeoutMillis
     * @return The {@link FileIdData} that was just locked
     */
    public @NonNull FileIdData implLock(@NonNull final String fileId, long newTimeoutMillis) {
        FileIdData fileIdData = null;

        synchronized (m_fileIdMap) {
            fileIdData = m_fileIdMap.get(fileId);

            if (null == fileIdData) {
                m_fileIdMap.put(fileId, fileIdData = new FileIdData(fileId));
            }
        }

        fileIdData.lock();
        fileIdData.setTimeoutMillis(newTimeoutMillis);

        return fileIdData;
    }

    /**
     * @param fileId
     * @return
     */
    public @NonNull FileIdData implUnlock(@NonNull final String fileId) {
        synchronized (m_fileIdMap) {
            final FileIdData fileIdData = m_fileIdMap.get(fileId);

            if (null != fileIdData) {
                fileIdData.unlock();
            }

            return fileIdData;
        }
    }

    /**
     *
     */
    protected void implCheckAutoCleanup() {
        final long curTimeMillis = System.currentTimeMillis();
        int originalCount = 0;
        int deletedCount = 0;

        // collect all entries to delete first
        synchronized (m_fileIdMap) {
            originalCount = m_fileIdMap.size();

            for (final String curKey : m_fileIdMap.keySet()) {
                final FileIdData curFileIdData = m_fileIdMap.get(curKey);

                // detect if timeout has been reached for FileItemData => schedule to delete
                if ((curTimeMillis - curFileIdData.getTimestampMillis()) >= curFileIdData.getTimeoutMillis()) {
                    // delete performs an asynchronous cleanup without synchronous wait or overhead
                    delete(curFileIdData.getFileId());
                    ++deletedCount;
                }
            }
        }

        if (DocumentConverterManager.isLogTrace() && (deletedCount > 0)) {
            DocumentConverterManager.logTrace(new StringBuilder(128).
                append("DC client FileIdManager auto cleanup scheduled ").
                append(deletedCount).append(" / ").
                append(originalCount).
                append(" entries for delete").toString());
        }
    }

    // - Static Members --------------------------------------------------------

    final private static String DC_FILEIDMANAGER_TIMER_NAME = "DC FileIdManager timer";

    private static FileIdManager fileIdManager = null;

    // - Members ---------------------------------------------------------------

    final protected AtomicBoolean m_isRunning = new AtomicBoolean(true);

    final protected DocumentConverterManager m_manager;

    final protected long m_fileIdDataTimeoutMillis;

    final protected Map<String, FileIdData> m_fileIdMap = new HashMap<>();

    final protected Timer m_autoCleanupCheckTimer = new Timer(DC_FILEIDMANAGER_TIMER_NAME, true);

    final protected ExecutorService m_deleteExecutorService = Executors.newSingleThreadExecutor();
}
