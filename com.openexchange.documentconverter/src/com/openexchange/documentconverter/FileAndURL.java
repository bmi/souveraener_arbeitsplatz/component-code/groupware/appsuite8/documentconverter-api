/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.io.File;

/**
 * {@link FileAndURL}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.5
 */
public class FileAndURL {

    /**
     * Initializes a new {@link FileAndURL}.
     */
    public FileAndURL() {
        super();

        m_file = null;
        m_url = null;
    }

    /**
     * Initializes a new {@link FileAndURL}.
     * @param file
     * @param url
     */
    public FileAndURL(final File file, final String url) {
        super();

        m_file = file;
        m_url = url;
    }

    /**
     * @return
     */
    final public File getFile() {
        return m_file;
    }

    /**
     * @return
     */
    final public String getURL() {
        return m_url;
    }

    // - Members -----------------------------------------------------------

    final private File m_file;

    final private String m_url;
}
