/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.net.ssl.SSLSocketFactoryProvider;

/**
 * {@link HttpHelper}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.6.0
 */
@SingletonService
public class HttpHelper {

    /**
     * {@link TransferDataMode}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.1
     */
    public enum TransferDataMode {
        BYTEARRAY_OBJECT,
        FILE_LENGTH_AND_DATA,
        OCTET_STREAM
    }

    /**
     * @param sslSocketfactoryProvider
     * @return
     */
    @NonNull public static synchronized HttpHelper newInstance(@NonNull final SSLSocketFactoryProvider sslSocketfactoryProvider) {
        if (null == m_httpHelper) {
            m_httpHelper = new HttpHelper(sslSocketfactoryProvider);
        }

        return m_httpHelper;
    }

    /**
     * @return
     */
    @Nullable public static synchronized HttpHelper get() {
        return m_httpHelper;
    }

    /**
     * Initializes a new {@link HttpHelper}.
     * @param sslSocketfactoryProvider
     */
    private HttpHelper(@NonNull final SSLSocketFactoryProvider sslSocketfactoryProvider) {
        super();

        m_sslSocketFactoryProvider = sslSocketfactoryProvider;
        m_sslSocketFactory = sslSocketfactoryProvider.getDefault();
    }

    /**
     * @return
     */
    public SSLContext getSSLContext() {
        return (null != m_sslSocketFactoryProvider) ? m_sslSocketFactoryProvider.getOriginatingDefaultContext() : null;
    }

    /**
     * @return
     */
    public SSLSocketFactory getSSLSocketFactory() {
        return m_sslSocketFactory;
    }

    /**
     * @param remoteUrl
     * @param transferObj
     */
    public void executeGet(@NonNull final URL remoteUrl, @NonNull final HttpResponseProcessor responseProcessor) throws Exception {

        HttpURLConnection httpConnection = implGetHttpConnection(remoteUrl);

        httpConnection.setRequestMethod("GET");
        httpConnection.setUseCaches(false);
        httpConnection.setConnectTimeout(5000);
        httpConnection.setReadTimeout(15000);

        httpConnection.connect();

        // process response
        try (final InputStream inputStm = httpConnection.getInputStream()) {
            if ((null != inputStm) && (null != responseProcessor)) {
                final int responseCode = httpConnection.getResponseCode();
                final String converterCookie = implGetConverterCookie(httpConnection);

                responseProcessor.processResponse(responseCode, inputStm, converterCookie);
            }
        } finally {
            httpConnection.disconnect();
        }

    }

    /**
     * @param remoteUrl
     * @param transferObj
     */
    public void postRequest(@NonNull final DocumentConverterManager manager,
        @NonNull final URL remoteUrl,
        @NonNull final ServerType serverType,
        @NonNull final TransferObject<? extends Serializable> transferObj,
        @NonNull final HttpResponseProcessor responseProcessor) throws Exception {

        final String queryString = transferObj.getQuery();

        if (null == queryString) {
            if (LOG.isTraceEnabled()) {
                LOG.trace("DC is not able to perform Http(s) request without query: " + remoteUrl.toString());
            }

            return;
        }

        HttpURLConnection httpConnection = null;

        try {
            httpConnection = implGetHttpConnection(remoteUrl);

            // initialize connection
            httpConnection.setRequestMethod("POST");
            httpConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpConnection.setRequestProperty("Content-Length", Integer.toString(queryString.length() + 1));
            httpConnection.setUseCaches(false);
            httpConnection.setDoOutput(true);
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setConnectTimeout(DocumentConverterManager.getHTTPConnectTimeoutMillis(serverType));
            httpConnection.setReadTimeout(DocumentConverterManager.getHTTPReadTimeoutMillis(serverType));

            // set Cookie if requested
            final String callCookie = transferObj.getCookie();

            if (null != callCookie) {
                httpConnection.setRequestProperty("Cookie", callCookie);
            }

            // establish Http(s) connection
            httpConnection.connect();

            // write to output stream
            try (final DataOutputStream dataOutputStm = new DataOutputStream(httpConnection.getOutputStream())) {
                dataOutputStm.writeBytes("?");
                dataOutputStm.writeBytes(queryString);

                dataOutputStm.flush();
            }

            // process response
            try (final InputStream inputStm = httpConnection.getInputStream()) {
                if ((null != inputStm) && (null != responseProcessor)) {
                    final int responseCode = httpConnection.getResponseCode();
                    final String converterCookie = implGetConverterCookie(httpConnection);

                    responseProcessor.processResponse(responseCode, inputStm, converterCookie);
                }
            }
        } finally {
            if (null != httpConnection) {
                httpConnection.disconnect();
            }
        }
    }

    /**
     * @param remoteUrl
     * @param transferObj
     */
    public void postFormData(@NonNull final DocumentConverterManager manager,
        @NonNull final URL remoteUrl,
        @NonNull final ServerType serverType,
        @NonNull final TransferObject<? extends Serializable> transferObj,
        @NonNull final TransferDataMode transferDataMode,
        @NonNull final HttpResponseProcessor responseProcessor)  throws Exception  {

        HttpURLConnection httpConnection = null;

        try {
            httpConnection = implGetHttpConnection(remoteUrl);

            final String boundary = StringUtils.remove(UUID.randomUUID().toString(), '-');
            final String partSeparator = FDATA_BDRY + boundary + FDATA_CRLF;

            // initialize connection
            httpConnection.setRequestMethod("POST");
            httpConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            httpConnection.setDoOutput(true);
            httpConnection.setUseCaches(false);
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setConnectTimeout(DocumentConverterManager.getHTTPConnectTimeoutMillis(serverType));
            httpConnection.setReadTimeout(DocumentConverterManager.getHTTPReadTimeoutMillis(serverType));

            // set Cookie if requested
            final String callCookie = transferObj.getCookie();

            if (null != callCookie) {
                httpConnection.setRequestProperty("Cookie", callCookie);
            }

            // perform the connection
            httpConnection.connect();

            try (final BufferedOutputStream bufferedOutputStm = new BufferedOutputStream(httpConnection.getOutputStream());
                final PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(bufferedOutputStm, "UTF-8"), false)) {
                final Map<String, String> paramMap = transferObj.getParamMap();

                if (null != paramMap) {
                    for (final String key : paramMap.keySet()) {
                        printWriter.append(partSeparator).
                            append("Content-Disposition: form-data; name=" + key).
                            append(FDATA_CRLF).
                            append(FDATA_CRLF).
                            append(paramMap.get(key)).
                            append(FDATA_CRLF);
                    }

                    printWriter.flush();
                }

                implWriteFileContent(printWriter, bufferedOutputStm, transferObj, partSeparator, transferDataMode);
                printWriter.flush();

                // mark end of multipart/form-data
                printWriter.
                    append(FDATA_BDRY + boundary + FDATA_BDRY).
                    append(FDATA_CRLF);

                printWriter.flush();
                bufferedOutputStm.flush();
            }

            try (final InputStream inputStm = httpConnection.getInputStream()) {
                if ((null != inputStm) && (null != responseProcessor)) {
                    final int responseCode = httpConnection.getResponseCode();
                    final String converterCookie = implGetConverterCookie(httpConnection);

                    responseProcessor.processResponse(responseCode, inputStm, converterCookie);
                }
            }
        } finally {
            if (null != httpConnection) {
                httpConnection.disconnect();
            }
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param url
     * @return
     * @throws IOException
     */
    @NonNull private HttpURLConnection implGetHttpConnection(@NonNull final URL url) throws IOException {
        URLConnection connection = url.openConnection();

        if (connection instanceof HttpsURLConnection) {
            ((HttpsURLConnection) connection).setSSLSocketFactory(m_sslSocketFactory);
        } else if (!(connection instanceof HttpURLConnection)) {
            connection = null;

            throw new IOException("DC is not able to establish Http(s) connection with URL: " + url.toString());
        }

        return (HttpURLConnection) connection;

    }

    /**
     * @param connection
     * @param responseProcessor
     */
    private String implGetConverterCookie(HttpURLConnection connection) {
        // extract cookies
        String converterCookie = null;
        int i = 0;

        while (true) {
            final String curHeaderFieldName = connection.getHeaderFieldKey(++i);

            if (null != curHeaderFieldName) {
                if (curHeaderFieldName.equalsIgnoreCase("set-cookie")) {
                    final String headerField = connection.getHeaderField(i);
                    final StringTokenizer tokenizer = new StringTokenizer(headerField, ";");

                    while (tokenizer.hasMoreTokens()) {
                        final String token = tokenizer.nextToken();
                        final int assignPos = token.indexOf('=');

                        if (-1 != assignPos) {
                            final String cookieKey = token.substring(0, assignPos);

                            if ((null != cookieKey) && cookieKey.trim().equalsIgnoreCase("jsessionid")) {
                                converterCookie = headerField;
                                break;
                            }
                        }
                    }

                    break;
                }
            } else {
                break;
            }
        }

        return converterCookie;
    }

    /**
     * @param printWriter
     * @param objOutputStm
     * @param transferObject
     * @param partSeparator
     */
    private void implWriteFileContent(PrintWriter printWriter, OutputStream outputStm, TransferObject<? extends Serializable> transferObject,
        String partSeparator, TransferDataMode transferDataMode) throws IOException {

        final Object contentSerializable = transferObject.getSerialObject();

        if (null != contentSerializable) {
            final String fieldName = (TransferDataMode.OCTET_STREAM == transferDataMode) ? "sourcefile" : "file";
            final String fileName = (null != transferObject.getFilename()) ? transferObject.getFilename() : "file.bin";
            final String mimeType = (null != transferObject.getMimeType()) ? transferObject.getMimeType() : "application/octet-stream";
            final StringBuilder multipartHeaderBuilder = new StringBuilder(1024).append(partSeparator);

            // Content disposition
            multipartHeaderBuilder.
                append("Content-Disposition: form-data").
                append(";name=\"").append(fieldName).append("\"").
                append(";filename=").append(fileName).
                append(FDATA_CRLF);

            // Content type
            multipartHeaderBuilder.
                append("Content-Type: ").append(mimeType).
                append(FDATA_CRLF);

            // Content-Transfer-Encoding
            multipartHeaderBuilder.
                append("Content-Transfer-Encoding: binary").
                append(FDATA_CRLF);

            // Finish multipart header
            multipartHeaderBuilder.
                append(FDATA_CRLF);

            // write multipart header
            printWriter.
                append(multipartHeaderBuilder).
                flush();

            // write data in different ways, depending on given PostDataMode
            if (contentSerializable instanceof File) {
                if (TransferDataMode.OCTET_STREAM == transferDataMode) {
                    // write content of source file as sequence of bytes into the output stream
                    FileUtils.copyFile((File) contentSerializable, outputStm);
                    outputStm.flush();
                } else {
                    final ObjectOutputStream objectOutputStm = new ObjectOutputStream(outputStm);

                    if (TransferDataMode.FILE_LENGTH_AND_DATA == transferDataMode) {
                        // write number of content bytes to write/read first
                        objectOutputStm.writeObject(Long.valueOf(((File) contentSerializable).length()));
                        objectOutputStm.flush();

                        // write content of source file as sequence of bytes into the output stream
                        FileUtils.copyFile((File) contentSerializable, objectOutputStm);
                    } else {
                        // write content of data file as byte array object
                        objectOutputStm.writeObject(FileUtils.readFileToByteArray((File) contentSerializable));
                    }

                    objectOutputStm.flush();
                }
            } else {
                final ObjectOutputStream objectOutputStm = new ObjectOutputStream(outputStm);

                if ((contentSerializable instanceof byte[]) && (TransferDataMode.OCTET_STREAM == transferDataMode)) {
                    objectOutputStm.write((byte[]) contentSerializable);
                } else {
                    objectOutputStm.writeObject(contentSerializable);
                }

                objectOutputStm.flush();
            }

            printWriter.
                append(FDATA_CRLF).
                flush();
        }
    }

    // - Static members --------------------------------------------------------

    final private static Logger LOG = LoggerFactory.getLogger(HttpHelper.class);

    final protected static String FDATA_BDRY = "--";

    final protected static  String FDATA_CRLF = "\r\n";

    private static HttpHelper m_httpHelper = null;

    // - Members

    final private SSLSocketFactoryProvider m_sslSocketFactoryProvider;

    final private SSLSocketFactory m_sslSocketFactory;
}
