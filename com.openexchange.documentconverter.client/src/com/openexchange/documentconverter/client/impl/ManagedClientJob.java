/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.impl;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.openexchange.documentconverter.DocumentConverterClientMetrics;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.DocumentConverterUtil;
import com.openexchange.documentconverter.DocumentInformation;
import com.openexchange.documentconverter.HttpHelper;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.JobErrorEx.StatusErrorCache;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.ServerType;
import com.openexchange.documentconverter.TransferObject;

/**
 * {@link ManagedClientJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
class ManagedClientJob {

    /**
     * Initializes a new {@link ManagedClientJob}.
     *
     * @param clientManager
     */
    ManagedClientJob(final ClientManager clientManager, final URL remoteUrl) {
        super();

        m_clientManager = clientManager;
        m_remoteUrl = remoteUrl;
    }

    /**
     * @param jobType
     * @param jobProperties
     * @param resultProperties
     * @return
     */
    boolean queryAvailability(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        boolean ret = false;

        if (implCheckJobPreconditions(jobProperties, resultProperties, 1)) {
            if (implPrepareRemoteCall(jobProperties, RCALL_QUERY_AVAILABILITY)) {
                jobProperties.put(Properties.PROP_JOBTYPE, jobType);

                implDoRemoteCall(jobProperties, resultProperties);

                ret = resultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE) &&
                    (JobErrorEx.fromResultProperties(resultProperties).hasNoError());
            }
        }

        return ret;
    }

    /**
     * @param jobType
     * @param jobProperties
     * @param resultProperties
     * @return
     */
    InputStream process(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        InputStream ret = null;
        final Boolean asyncObject = (Boolean) jobProperties.get(Properties.PROP_ASYNC);

        if ((null != asyncObject) && asyncObject.booleanValue()) {
            if (implCheckJobInputFileSize(jobProperties, resultProperties) &&
                implCheckSupportedJobInputType(jobProperties, resultProperties)) {

                m_clientManager.triggerAsyncConvert(jobType, jobProperties, resultProperties);
            }
        } else if (implCheckJobPreconditions(jobProperties, resultProperties, 1)) {
            if (implPrepareRemoteCall(jobProperties, RCALL_CONVERT)) {
                jobProperties.put(Properties.PROP_JOBTYPE, jobType);
                ret = (InputStream) implDoRemoteCall(jobProperties, resultProperties);
            }
        }

        return ret;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param jobProperties
     * @param resultProperties
     * @return
     */
    private byte[] implGetInputBuffer(final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties) {
        byte[] ret = null;

        try (final InputStream inputStm = (jobProperties.containsKey(Properties.PROP_INPUT_FILE) ?
            new FileInputStream((File) jobProperties.get(com.openexchange.documentconverter.Properties.PROP_INPUT_FILE)) :
                (InputStream) jobProperties.get(Properties.PROP_INPUT_STREAM))) {

            if (null != inputStm) {
                ret = IOUtils.toByteArray(inputStm);
            }
        } catch (final Exception e) {
            DocumentConverterManager.logExcp(e);
        }

        // check, if length of byte array is larger than specified config source file size
        if ((null != ret) && (implSetSourceFileSizeError(ret.length, jobProperties, resultProperties))) {
            ret = null;
        }

        return ret;
    }

    /**
     * @param jobProperties
     */
    /**
     * @param jobProperties
     * @param resultProperties
     * @param minRemoteAPIVersion
     * @return true, if job processing can proceed
     */
    private boolean implCheckJobPreconditions(final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties, int minRemoteAPIVersion) {
        boolean ret = false;

        if (null != jobProperties) {
            // job is valid by default, if job properties exists at all
            // and source file size fits into the given constraints
            ret = implCheckJobInputFileSize(jobProperties, resultProperties) &&
                implCheckSupportedJobInputType(jobProperties, resultProperties);
        } else {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
        }

        if (ret && (null != m_remoteUrl)) {
            if (!(ret = m_clientManager.getRemoteAPIVersion(false) >= minRemoteAPIVersion)) {
                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.SERVER_CONNECTION_ERROR.getErrorCode()));
            }
        }

        if (!ret) {
            final JobErrorEx jobErrorEx = JobErrorEx.fromResultProperties(resultProperties);
            final String fileName = (null != jobProperties) ? ((String) jobProperties.get(Properties.PROP_INFO_FILENAME)) : null;

            DocumentConverterManager.logWarn("DC client remote conversion failed",
                new LogData("joberror", jobErrorEx.getJobError().getErrorText()),
                new LogData("filename", (null != fileName) ? fileName : DocumentConverterUtil.STR_UNKNOWN));
        }

        return ret;
    }

    /**
     * @param jobProperties
     * @param resultProperties
     * @return true, if source file size is unlimited or below/equal
     *  to the given maximum source file size
     */
    private boolean implCheckJobInputFileSize(final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties) {
        boolean ret = false;

        if (null != jobProperties) {
            // check max source file size, if source file is given
            final File inputFile = (File) jobProperties.get(Properties.PROP_INPUT_FILE);

            if (null != inputFile) {
                // check maxSourceFile
                ret = !implSetSourceFileSizeError(inputFile.length(), jobProperties, resultProperties);
            } else {
                // job is valid by default, if job properties exists
                // at all and source file is given as input stream
                ret = true;
            }
        } else {
            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
        }

        return ret;
    }

    /**
     * @param jobProperties
     * @param resultProperties
     * @return true, if source file size is unlimited or below/equal
     *  to the given maximum source file size
     */
    private boolean implCheckSupportedJobInputType(final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties) {
        boolean ret = false;

        if (null != jobProperties) {
            // check max source file size, if source file is given
            String inputType = (String) jobProperties.get(Properties.PROP_INPUT_TYPE);

            if (StringUtils.isEmpty(inputType)) {
                final String fileName = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);

                if (StringUtils.isNotEmpty(fileName)) {
                    inputType = FilenameUtils.getExtension(fileName);

                    if (StringUtils.isNotEmpty(inputType)) {
                        inputType = inputType.trim().toLowerCase();
                    }
                }
            }

            if (StringUtils.isEmpty(inputType)) {
                inputType = "bin";
            }

            ret = !implSetSupportedTypeError(inputType, jobProperties, resultProperties);
        }

        return ret;
    }

    /**
     * @param sourceSize
     * @param maxSize
     * @param resultProperties
     * @return true, if source size is larger than max size
     */
    private boolean implSetSourceFileSizeError(long sourceSize, final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties) {
        final long maxSize = m_clientManager.getClientConfig().MAX_DOCUMENT_SOURCEFILESIZE;
        boolean ret = false;

        if (sourceSize > maxSize) {
            ret = true;

            if (null != resultProperties) {
                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.MAX_SOURCESIZE.getErrorCode()));

                if (DocumentConverterManager.isLogError()) {
                    DocumentConverterManager.logError("DC client detected source file size larger than configured size",
                        jobProperties,
                        new LogData("sourcesize", Long.valueOf(sourceSize).toString()),
                        new LogData("maxsize", Long.valueOf(maxSize).toString()));
                }
            }
        }

        return ret;
    }

    /**
     * @param sourceSize
     * @param maxSize
     * @param resultProperties
     * @return true, if source size is larger than max size
     */
    private boolean implSetSupportedTypeError(String inputType, final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties) {
        final Set<String> supportedTypes = m_clientManager.getClientConfig().SUPPORTED_TYPES;
        final boolean ret = (supportedTypes.size() > 0) &&
            DocumentInformation.isExtensionSupported(inputType) &&
            !supportedTypes.contains(inputType);

        if (ret) {
            if (null != resultProperties) {
                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.UNSUPPORTED.getErrorCode()));
            }

            if (DocumentConverterManager.isLogError()) {
                DocumentConverterManager.logError("DC client detected unsupported input file extension",
                    jobProperties, new LogData("inputype", inputType));
            }
        }

        return ret;
    }

    /**
     * @param jobProperties
     * @param method
     * @return
     */
    private boolean implPrepareRemoteCall(HashMap<String, Object> jobProperties, String method) {
        final String calledRemoteUrlString = (String) jobProperties.get(Properties.PROP_ENGINE_REMOTEURL);
        URL calledRemoteUrl = null;
        boolean ret = false;

        if (!StringUtils.isEmpty(calledRemoteUrlString)) {
            try {
                calledRemoteUrl = new URL(calledRemoteUrlString);
            } catch (final MalformedURLException e) {
                DocumentConverterManager.logExcp(e);
            }
        }

        // check if a remote conversion is requested and if we're not remotely called by ourselves
        if ((null != m_remoteUrl) && ((null == calledRemoteUrl) || !calledRemoteUrl.equals(m_remoteUrl))) {
            jobProperties.put(Properties.PROP_REMOTE_METHOD, method);
            jobProperties.put(Properties.PROP_ENGINE_REMOTEURL, m_remoteUrl.toString());
            ret = true;
        }

        // get stored cookie from previous beginPage call and add to job properties, if possible
        if (method.equals(RCALL_GETCONVERSIONPAGE) || method.equals(RCALL_ENDPAGECONVERSION)) {
            final String converterJobId = (String) jobProperties.get(Properties.PROP_JOBID);

            if (StringUtils.isNotEmpty(converterJobId)) {
                final String converterCookie = m_converterCookieMap.get(converterJobId);

                if (StringUtils.isNotEmpty(converterCookie)) {
                    jobProperties.put(Properties.PROP_CONVERTER_COOKIE, converterCookie);
                }
            }
        }

        return ret;
    }

    /**
     * @param jobProperties
     * @param resultProperties
     * @param jobType
     * @return
     */
    private Object implDoRemoteCall(final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties) {
        Object ret = null;
        final boolean isMultipartRequest = (jobProperties.containsKey(Properties.PROP_INPUT_FILE) || jobProperties.containsKey(Properties.PROP_INPUT_STREAM));
        final Boolean asyncJob = (Boolean) jobProperties.get(Properties.PROP_ASYNC);
        final Boolean remoteAsyncJob = (Boolean) jobProperties.get(Properties.PROP_REMOTE_ASYNC);
        final Boolean cacheOnlyJob = (Boolean) jobProperties.get(Properties.PROP_CACHE_ONLY);
        final String remoteCacheHash = (String) jobProperties.get(Properties.PROP_REMOTE_CACHE_HASH);
        final boolean isAsync = ((null != asyncJob) && asyncJob.booleanValue()) || ((null != remoteAsyncJob) && remoteAsyncJob.booleanValue());
        final boolean isCacheOnly = (null != cacheOnlyJob) && cacheOnlyJob.booleanValue();
        final String remoteParams = isMultipartRequest ? implGetRemoteParams(jobProperties, resultProperties, false, "returntype=binary") : implGetRemoteParams(jobProperties, resultProperties, true);
        final boolean logTrace = DocumentConverterManager.isLogTrace();
        final String remoteMethod = jobProperties.containsKey(Properties.PROP_REMOTE_METHOD) ? (String) jobProperties.get(Properties.PROP_REMOTE_METHOD) : "";
        final String logTitle = "transformimage".equals(remoteMethod) ?
            ("ImageServer " + (isAsync ? "asynchronous " : "") + "remote transformation") :
                ("DC client " + (isAsync ? "asynchronous " : "") + "remote conversion");

            if ((null != m_remoteUrl) && (null != remoteParams)) {
                final String callCookie = (String) jobProperties.get(Properties.PROP_CONVERTER_COOKIE);
                URL remoteUrl = null;

                if (logTrace) {
                    DocumentConverterManager.logTrace(logTitle + " initiated", jobProperties, new LogData("method", remoteMethod));
                }

                try {
                    remoteUrl = new URL(m_remoteUrl.toString() + "?action=rconvert");
                } catch (final MalformedURLException e) {
                    DocumentConverterManager.logExcp(e);
                    return ret;
                }

                try {
                    if (isMultipartRequest) {
                        final String inputType = (String) jobProperties.get(Properties.PROP_INPUT_TYPE);
                        final String inputMimeType = "pdf".equals(inputType) ? "application/pdf" : "application/octet-stream";
                        final Serializable contentSerializable =  jobProperties.containsKey(Properties.PROP_INPUT_FILE) ?
                            (File) jobProperties.get(Properties.PROP_INPUT_FILE) :
                                implGetInputBuffer(jobProperties, resultProperties);


                        if (null != contentSerializable) {
                            final int remoteAPIVersion = m_clientManager.getRemoteAPIVersion(false);
                            final TransferObject<Serializable> transferObject = new TransferObject<>(
                                remoteParams,
                                contentSerializable,
                                "ByteArray.bin",
                                inputMimeType,
                                callCookie);
                            final HttpHelper.TransferDataMode transferDataMode =
                                (remoteAPIVersion >= 8) ?
                                    HttpHelper.TransferDataMode.OCTET_STREAM :
                                        (((contentSerializable instanceof File) && (remoteAPIVersion >= 6)) ?
                                            HttpHelper.TransferDataMode.FILE_LENGTH_AND_DATA :
                                                HttpHelper.TransferDataMode.BYTEARRAY_OBJECT);

                            HttpHelper.get().postFormData(
                                m_clientManager,
                                remoteUrl,
                                ServerType.DOCUMENTCONVERTER,
                                transferObject,
                                transferDataMode,
                                (int responseCode, InputStream responseInputStream, String resultCookie) -> {
                                    // evaluate result only for synchronous calls with valid result stream
                                    if (!isAsync) {
                                        try (ObjectInputStream objInputStream = new ObjectInputStream(new BufferedInputStream(responseInputStream))) {
                                            final Object readObject = objInputStream.readObject();

                                            if (readObject instanceof HashMap<?, ?>) {
                                                resultProperties.clear();
                                                resultProperties.putAll((HashMap<? extends String, ?>) readObject);

                                                if (null != resultCookie) {
                                                    resultProperties.put(Properties.PROP_RESULT_CONVERTER_COOKIE, resultCookie);
                                                }

                                                // check for old style property and convert to result error
                                                final Boolean passwordProtected = (Boolean) resultProperties.get("PasswordProtected");

                                                if ((null != passwordProtected) && passwordProtected.booleanValue()) {
                                                    resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.PASSWORD.getErrorCode()));
                                                }
                                            }

                                            m_clientManager.getDocumentConverterClientMetrics().
                                                incrementConnectionStatusCount(DocumentConverterClientMetrics.ConnectionType.CONVERT, null);
                                        } catch (final Exception e) {
                                            resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.SERVER_CONNECTION_ERROR.getErrorCode()));

                                            m_clientManager.getDocumentConverterClientMetrics().
                                                incrementConnectionStatusCount(DocumentConverterClientMetrics.ConnectionType.CONVERT, e);

                                            DocumentConverterManager.logExcp(e);
                                        }
                                    }
                                });

                            final byte[] resultBuffer = (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER);

                            if (null != resultBuffer) {
                                ret = new ByteArrayInputStream(resultBuffer);
                            }
                        }
                    } else {
                        final TransferObject<byte[]> transferObject = new TransferObject<>(remoteParams, callCookie);
                        final StringBuilder response = new StringBuilder();

                        HttpHelper.get().postRequest(
                            m_clientManager,
                            remoteUrl,
                            ServerType.DOCUMENTCONVERTER,
                            transferObject,
                            (int responseCode, InputStream responseInputStream, String resultCookie) -> {
                                // evaluate result only for synchronous calls with valid result stream
                                if (!isAsync) {
                                    try (BufferedReader inputReader = new BufferedReader(new InputStreamReader(responseInputStream))) {
                                        for (String readLine = null; (readLine = inputReader.readLine()) != null;) {
                                            response.append(readLine).append('\n');
                                        }

                                        if (null != resultCookie) {
                                            resultProperties.put(Properties.PROP_RESULT_CONVERTER_COOKIE, resultCookie);
                                        }

                                        m_clientManager.getDocumentConverterClientMetrics().
                                            incrementConnectionStatusCount(DocumentConverterClientMetrics.ConnectionType.CONVERT, null);
                                    } catch (final Exception e) {
                                        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.SERVER_CONNECTION_ERROR.getErrorCode()));

                                        m_clientManager.getDocumentConverterClientMetrics().
                                            incrementConnectionStatusCount(DocumentConverterClientMetrics.ConnectionType.CONVERT, e);

                                        DocumentConverterManager.logExcp(e);
                                    }
                                }
                            });

                        // parse JSON response only in case of synchronous jobs
                        if (!isAsync) {
                            try {
                                ret = implParseJSONResponse(new JSONObject(new JSONTokener(response.toString())), resultProperties);
                            } catch (final JSONException e) {
                                DocumentConverterManager.logExcp(e);
                            }
                        }
                    }
                } catch (Exception e) {
                    resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.SERVER_CONNECTION_ERROR.getErrorCode()));

                    DocumentConverterManager.logExcp("DC client received exception during remote call", e);

                    m_clientManager.getDocumentConverterClientMetrics().
                        incrementConnectionStatusCount(DocumentConverterClientMetrics.ConnectionType.CONVERT, e);

                    m_clientManager.resetRemoteConnection();

                }
            }

            if (isAsync) {
                if (logTrace) {
                    DocumentConverterManager.logTrace(logTitle + " implicitly succeeded", jobProperties);
                }
            } else if (JobErrorEx.fromResultProperties(resultProperties).hasNoError()) {
                if (logTrace) {
                    DocumentConverterManager.logTrace(logTitle + " succeeded", jobProperties);
                }
            } else if (!isCacheOnly && !isAsync && StringUtils.isEmpty(remoteCacheHash)) {
                // don't log warnings for cacheOnly jobs
                // don't log warnings for async jobs
                // don't log warnings for remote cachehash jobs
                // ------------------------------------------------------
                // all above mentioned types of jobs may return without a
                // valid or with an error result. This should not be logged
                // as an error/warning at all here, since there'll either be a
                // fallback conversion or the caller is not interested in the
                // the current result of these 'try and ignore' conversions
                final JobErrorEx jobErrorEx = JobErrorEx.fromResultProperties(resultProperties);
                final StatusErrorCache statusErrorCache = jobErrorEx.getStatusErrorCache();
                final List<LogData> logDataList = new ArrayList<>();

                logDataList.add(new LogData("joberror", jobErrorEx.getJobError().getErrorText()));

                if ((null != statusErrorCache) && (StatusErrorCache.NONE != statusErrorCache)) {
                    logDataList.add(new LogData("statusErrorCache", statusErrorCache.getStatusText()));
                }

                logDataList.add(new LogData("filename", DocumentConverterManager.getFilenameForLog((String) jobProperties.get(Properties.PROP_INFO_FILENAME))));

                DocumentConverterManager.logWarn("DC client remote conversion failed", logDataList.toArray(new LogData[logDataList.size()]));
            }

            return ret;
    }

    /**
     * @param jobType
     * @param jobProperties
     * @return
     */
    private String implGetRemoteParams(final HashMap<String, Object> jobProperties, final HashMap<String, Object> resultProperties, boolean includeDataUrl, String... additionalParams) {
        StringBuilder paramBuilder = new StringBuilder("action=rconvert");
        Object curParam = null;

        if (null != jobProperties) {
            // MethodName
            if (null != (curParam = jobProperties.get(Properties.PROP_REMOTE_METHOD))) {
                paramBuilder.append("&method=").append((String) curParam);
            }

            // JobType
            if (null != (curParam = jobProperties.get(Properties.PROP_JOBTYPE))) {
                paramBuilder.append("&jobtype=").append((String) curParam);
            }

            // JobId
            if (null != (curParam = jobProperties.get(Properties.PROP_JOBID))) {
                paramBuilder.append("&jobid=").append((String) curParam);
            }

            // JobPriority
            if (null != (curParam = jobProperties.get(Properties.PROP_PRIORITY))) {
                paramBuilder.append("&priority=").append(((JobPriority) curParam).toString());
            }

            // Locale
            if (null != (curParam = jobProperties.get(Properties.PROP_LOCALE))) {
                paramBuilder.append("&locale=").append((String) curParam);
            }

            // CacheHash
            if (null != (curParam = jobProperties.get(Properties.PROP_CACHE_HASH))) {
                paramBuilder.append("&cachehash=").append((String) curParam);
            }

            // RemoteCacheHash
            if (null != (curParam = jobProperties.get(Properties.PROP_REMOTE_CACHE_HASH))) {
                paramBuilder.append("&remotecachehash=").append((String) curParam);
            }

            // FilterShortName
            if (null != (curParam = jobProperties.get(Properties.PROP_FILTER_SHORT_NAME))) {
                paramBuilder.append("&filtershortname=").append((String) curParam);
            }

            // InputType
            if (null != (curParam = jobProperties.get(Properties.PROP_INPUT_TYPE))) {
                paramBuilder.append("&inputtype=").append((String) curParam);
            }

            // InputURL
            if (null != (curParam = jobProperties.get(Properties.PROP_INPUT_URL))) {
                final String inputURL = (String) curParam;

                try {
                    paramBuilder.append("&inputurl=").append(URLEncoder.encode(inputURL, "UTF-8"));
                } catch (final UnsupportedEncodingException e) {
                    DocumentConverterManager.logExcp(e);
                }
            }

            // PixelX
            if (null != (curParam = jobProperties.get(Properties.PROP_PIXEL_X))) {
                paramBuilder.append("&pixelx=").append(((Integer) curParam).toString());
            }

            // PixelY
            if (null != (curParam = jobProperties.get(Properties.PROP_PIXEL_Y))) {
                paramBuilder.append("&pixely=").append(((Integer) curParam).toString());
            }

            // PixelWidth
            if (null != (curParam = jobProperties.get(Properties.PROP_PIXEL_WIDTH))) {
                paramBuilder.append("&pixelwidth=").append(((Integer) curParam).toString());
            }

            // PixelHeight
            if (null != (curParam = jobProperties.get(Properties.PROP_PIXEL_HEIGHT))) {
                paramBuilder.append("&pixelheight=").append(((Integer) curParam).toString());
            }

            // MimeType
            if (null != (curParam = jobProperties.get(Properties.PROP_MIME_TYPE))) {
                paramBuilder.append("&mimetype=").append((String) curParam);
            }

            // PageRange
            if (null != (curParam = jobProperties.get(Properties.PROP_PAGE_RANGE))) {
                paramBuilder.append("&pagerange=").append((String) curParam);
            }

            // PageNumber
            if (null != (curParam = jobProperties.get(Properties.PROP_PAGE_NUMBER))) {
                paramBuilder.append("&pagenumber=").append(((Integer) curParam).toString());
            }

            // ShapeNumber
            if (null != (curParam = jobProperties.get(Properties.PROP_SHAPE_NUMBER))) {
                paramBuilder.append("&shapenumber=").append(((Integer) curParam).toString());
            }

            // ZipArchive
            if (null != (curParam = jobProperties.get(Properties.PROP_ZIP_ARCHIVE))) {
                paramBuilder.append("&ziparchive=").append(((Boolean) curParam).toString());
            }

            // PROP_INFO_FILENAME
            if (null != (curParam = jobProperties.get(Properties.PROP_INFO_FILENAME))) {
                final String infoFileName = (String) curParam;

                try {
                    paramBuilder.append("&infofilename=").append(URLEncoder.encode(infoFileName, "UTF-8"));
                } catch (final UnsupportedEncodingException e) {
                    DocumentConverterManager.logExcp(e);
                }
            }

            // Feature
            if (null != (curParam = jobProperties.get(Properties.PROP_FEATURES_ID))) {
                paramBuilder.append("&featuresid=").append(((Integer) curParam).toString());
            }

            // CacheOnly
            if (null != (curParam = jobProperties.get(Properties.PROP_CACHE_ONLY))) {
                paramBuilder.append("&cacheonly=").append(((Boolean) curParam).toString());
            }

            // NoCache
            if (null != (curParam = jobProperties.get(Properties.PROP_NO_CACHE))) {
                paramBuilder.append("&nocache=").append(((Boolean) curParam).toString());
            }

            // HideChanges
            if (null != (curParam = jobProperties.get(Properties.PROP_HIDE_CHANGES))) {
                paramBuilder.append("&hidechanges=").append(((Boolean) curParam).toString());
            }

            // HideComments
            if (null != (curParam = jobProperties.get(Properties.PROP_HIDE_COMMENTS))) {
                paramBuilder.append("&hidecomments=").append(((Boolean) curParam).toString());
            }

            // Async
            if (null != (curParam = jobProperties.get(Properties.PROP_ASYNC))) {
                paramBuilder.append("&async=").append(((Boolean) curParam).toString());
            }

            // RemoteAsync (treated as async for remote side)
            if (null != (curParam = jobProperties.get(Properties.PROP_REMOTE_ASYNC))) {
                paramBuilder.append("&async=").append(((Boolean) curParam).toString());
            }

            // IsAvailable
            if (null != (curParam = jobProperties.get(Properties.PROP_QUERY_AVAILABILITY))) {
                paramBuilder.append("&queryavailability=").append(((Boolean) curParam).toString());
            }

            // ImageResolution
            if (null != (curParam = jobProperties.get(Properties.PROP_IMAGE_RESOLUTION))) {
                paramBuilder.append("&imageresolution=").append(((Integer) curParam).toString());
            }

            // ScaleType
            if (null != (curParam = jobProperties.get(Properties.PROP_IMAGE_SCALE_TYPE))) {
                paramBuilder.append("&imagescaletype=").append((String) curParam);
            }

            // UserRequest
            if (null != (curParam = jobProperties.get(Properties.PROP_USER_REQUEST))) {
                paramBuilder.append("&userrequest=").append(((Boolean) curParam).toString());
            }

            // RemoteUrl
            paramBuilder.append("&remoteurl=").append(m_remoteUrl.toString());

            // ShapeReplacements
            if (null != (curParam = jobProperties.get(Properties.PROP_SHAPE_REPLACEMENTS))) {
                try {
                    paramBuilder.append("&shapereplacements=").append(URLEncoder.encode((String) curParam, "UTF-8"));
                } catch (final UnsupportedEncodingException e) {
                    DocumentConverterManager.logExcp(e);
                }
            }

            // add additional params, if available
            if (null != additionalParams) {
                for (final String curParamStr : additionalParams) {
                    if ((null != curParamStr) && (curParamStr.indexOf('=') > 0)) {
                        paramBuilder.append('&').append(curParamStr);
                    }
                }
            }

            if (includeDataUrl) {
                // add base64 encoded data from InputFile or InputStream as data Url
                final byte[] inputBuffer = implGetInputBuffer(jobProperties, resultProperties);

                if (null != inputBuffer) {
                    final String inputType = (String) jobProperties.get(Properties.PROP_INPUT_TYPE);
                    final String inputMimeType = "pdf".equals(inputType) ? "application/pdf" : "application/octet-stream";

                    try {
                        final String base64String = Base64.encodeBase64URLSafeString(inputBuffer);

                        if (null != base64String) {
                            paramBuilder.append("&url=data:").append(inputMimeType).append(";base64,").append(base64String);
                        }
                    } catch (final Exception e) {
                        DocumentConverterManager.logExcp(e);
                        paramBuilder = null;
                    }
                }
            }
        }

        return ((null != paramBuilder) ? paramBuilder.toString() : null);
    }

    /**
     * @param jsonObject
     * @return
     */
    private Object implParseJSONResponse(JSONObject jsonObject, HashMap<String, Object> resultProperties) {
        Object ret = null;

        try {
            if (null != jsonObject) {
                final Set<String> keys = jsonObject.keySet();

                if (keys.contains("errorcode")) {
                    resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(jsonObject.getInt("errorcode")));
                }

                if (keys.contains("errordata")) {
                    resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, jsonObject.getString("errordata"));
                }

                if (keys.contains("cachehash")) {
                    resultProperties.put(Properties.PROP_RESULT_CACHE_HASH, jsonObject.getString("cachehash"));
                }

                if (keys.contains("inputfilehash")) {
                    resultProperties.put(Properties.PROP_RESULT_INPUTFILE_HASH, jsonObject.getString("inputfilehash"));
                }

                if (keys.contains("locale")) {
                    resultProperties.put(Properties.PROP_RESULT_LOCALE, jsonObject.getString("locale"));
                }

                if (keys.contains("jobid")) {
                    resultProperties.put(Properties.PROP_RESULT_JOBID, jsonObject.getString("jobid"));
                }

                if (keys.contains("pagecount")) {
                    resultProperties.put(Properties.PROP_RESULT_PAGE_COUNT, Integer.valueOf(jsonObject.getInt("pagecount")));
                }

                if (keys.contains("originalpagecount")) {
                    resultProperties.put(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT, Integer.valueOf(jsonObject.getInt("originalpagecount")));
                }

                if (keys.contains("pagenumber")) {
                    resultProperties.put(Properties.PROP_RESULT_PAGE_NUMBER, Integer.valueOf(jsonObject.getInt("pagenumber")));
                }

                if (keys.contains("shapenumber")) {
                    resultProperties.put(Properties.PROP_RESULT_SHAPE_NUMBER, Integer.valueOf(jsonObject.getInt("shapenumber")));
                }

                if (keys.contains("mimetype")) {
                    resultProperties.put(Properties.PROP_RESULT_MIME_TYPE, jsonObject.getString("mimetype"));
                }

                if (keys.contains("extension")) {
                    resultProperties.put(Properties.PROP_RESULT_EXTENSION, jsonObject.getString("extension"));
                }

                if (keys.contains("passwordprotected") && jsonObject.getBoolean("passwordprotected")) {
                    resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.PASSWORD.getErrorCode()));
                }

                if (keys.contains("maxsourcesize") && jsonObject.getBoolean("maxsourcesize")) {
                    resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.MAX_SOURCESIZE.getErrorCode()));
                }

                if (keys.contains("unsupported") && jsonObject.getBoolean("unsupported")) {
                    resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.UNSUPPORTED.getErrorCode()));
                }

                if (keys.contains("result")) {
                    String resultDataUrl = jsonObject.getString("result");

                    // we're done with the JSON object, so close it ASAP
                    // in order to save memory for the upcoming decoding process
                    jsonObject.reset();

                    if (resultDataUrl.startsWith("data:")) {
                        int pos = resultDataUrl.indexOf(BASE64_PATTERN);

                        if ((-1 != pos) && (pos > 5)) {
                            if (resultDataUrl.substring(5, 5 + ZIP_MIMETYPE.length()).equals(ZIP_MIMETYPE)) {
                                resultProperties.put(Properties.PROP_RESULT_ZIP_ARCHIVE, Boolean.TRUE);
                            }
                        }

                        if ((-1 != pos) && ((pos += BASE64_PATTERN.length()) < (resultDataUrl.length() - 1))) {
                            final byte[] data = Base64.decodeBase64(resultDataUrl.substring(pos));

                            // reset early since we're done with this string
                            resultDataUrl = null;

                            if ((null != data) && (data.length > 0)) {
                                resultProperties.put(Properties.PROP_RESULT_BUFFER, data);
                                ret = new ByteArrayInputStream(data);
                            }
                        }
                    }
                }
            }
        } catch (final JSONException e) {
            DocumentConverterManager.logExcp(e);
        }

        return ret;
    }

    // - Members ---------------------------------------------------------------

    protected ClientManager m_clientManager = null;

    protected volatile boolean m_curJobDone = false;

    protected volatile boolean m_jobRunningStateReached = false;

    protected volatile long m_jobStartExecTimeMillis = 0;

    protected URL m_remoteUrl = null;

    // - Static members ---------------------------------------------------------

    protected static final String ZIP_MIMETYPE = "application/zip";

    protected static final String BASE64_PATTERN = "base64,";

    protected static final String RCALL_CONVERT = "convert";

    protected static final String RCALL_QUERY_AVAILABILITY = "queryavailability";

    protected static final String RCALL_BEGINPAGECONVERSION = "beginpageconversion";

    protected static final String RCALL_GETCONVERSIONPAGE = "getconversionpage";

    protected static final String RCALL_ENDPAGECONVERSION = "endpageconversion";

    protected static Map<String, String> m_converterCookieMap = new ConcurrentHashMap<>();
}
