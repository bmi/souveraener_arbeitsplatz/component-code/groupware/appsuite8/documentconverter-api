/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * {@link Properties}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class Properties {

    static final public String INFO_PREFIX = "info_";

    // -------------------------------------------------------------------------

    static final public int MAX_ERROR_MESSAGE_TOTAL_LENGTH = 2048;

    static final public int MAX_ERROR_MESSAGE_END_LENGTH = 256;

    // -------------------------------------------------------------------------

    static final public String CACHE_PROPERTIES_FILENAME = "properties"; // String

    static final public String CACHE_RESULT_FILENAME = "result"; // String

    static final public String CACHE_NO_CACHEHASH = "0";

    // -------------------------------------------------------------------------

    static final public String PROP_CACHE_PERSIST_ENTRY_NAME = "EntryName"; // String

    static final public String PROP_CACHE_HASH = "CacheHash"; // String

    static final public String PROP_REMOTE_CACHE_HASH = "RemoteCacheHash"; // String

    static final public String PROP_INPUTFILE_HASH = "InputFileHash"; // String

    static final public String PROP_LOCALE = "Locale"; // String

    static final public String PROP_FILTER_SHORT_NAME = "FilterShortName"; // String

    static final public String PROP_IDOCUMENTCONVERTER = "IDocumentConverter"; // IDocumentConverter

    static final public String PROP_PIXEL_X = "PixelX"; // Integer

    static final public String PROP_PIXEL_Y = "PixelY"; // Integer

    static final public String PROP_PIXEL_WIDTH = "PixelWidth"; // Integer

    static final public String PROP_PIXEL_HEIGHT = "PixelHeight"; // Integer

    static final public String PROP_PIXEL_ZOOM= "PixelZoom"; // Double

    static final public String PROP_INPUT_FILE = "InputFile"; // File

    static final public String PROP_INPUT_STREAM = "InputStream"; // InputStream

    static final public String PROP_INPUT_TYPE = "InputType"; // String

    static final public String PROP_INPUT_URL = "InputUrl"; // String

    static final public String PROP_JOBID = "JobId"; // String

    static final public String PROP_JOBTYPE = "JobType"; // String

    static final public String PROP_MIME_TYPE = "MimeType"; // String

    static final public String PROP_OUTPUT_FILE = "OutputFile"; // File

    static final public String PROP_PAGE_NUMBER = "PageNumber"; // Integer

    static final public String PROP_SHAPE_NUMBER = "ShapeNumber"; // Integer

    static final public String PROP_PAGE_RANGE = "PageRange"; // String

    static final public String PROP_ZIP_ARCHIVE = "ZipArchive"; // Boolean

    static final public String PROP_PRIORITY = "Priority"; // JobPriority

    static final public String PROP_READERENGINE_ROOT = "ReaderEngineRoot"; // String

    static final public String PROP_ENGINE_REMOTEURL = "EngineRemoteUrl"; // String

    static final public String PROP_REMOTE_METHOD = "Method"; // String

    static final public String PROP_INFO_FILENAME = INFO_PREFIX + "Filename"; // String

    static final public String PROP_FEATURES_ID = "FeaturesId"; // Integer

    static final public String PROP_CONVERTER_COOKIE = "ConverterCookie"; // String

    static final public String PROP_CACHE_ONLY = "CacheOnly"; // Boolean

    static final public String PROP_NO_CACHE = "NoCache"; // Boolean

    static final public String PROP_AUTOSCALE = "AutoScale"; // Boolean

    static final public String PROP_HIDE_CHANGES = "HideChanges"; // Boolean

    static final public String PROP_HIDE_COMMENTS = "HideComments"; // Boolean

    static final public String PROP_ASYNC = "Async"; // Boolean

    static final public String PROP_REMOTE_ASYNC = "RemoteAsync"; // Boolean

    static final public String PROP_QUERY_AVAILABILITY = "QueryAvailability"; // Boolean

    static final public String PROP_IMAGE_SCALE_TYPE = "ImageScaleType"; // String

    static final public String PROP_IMAGE_RESOLUTION = "ImageResolution"; // Integer

    static final public String PROP_USER_REQUEST = "UserRequest"; // Boolean

    static final public String PROP_CLOSE_DOCUMENT = "CloseDocument"; // Boolean

    static final public String PROP_SHAPE_REPLACEMENTS = "ShapeReplacements"; // String

    // -------------------------------------------------------------------------

    static final public String PROP_RESULT_CACHE_HASH = PROP_CACHE_HASH; // String

    static final public String PROP_RESULT_INPUTFILE_HASH = PROP_INPUTFILE_HASH; // String

    static final public String PROP_RESULT_LOCALE = PROP_LOCALE; // String

    static final public String PROP_RESULT_ERROR_CODE = "ErrorCode"; // Integer

    static final public String PROP_RESULT_ERROR_DATA = "ErrorData"; // String

    static final public String PROP_RESULT_EXTENSION = "Extension"; // String

    static final public String PROP_RESULT_BUFFER = "ResultBuffer"; // byte[]

    static final public String PROP_RESULT_JOBID = PROP_JOBID; // String

    static final public String PROP_RESULT_MIME_TYPE = PROP_MIME_TYPE; // String

    static final public String PROP_RESULT_PAGE_COUNT = "PageCount"; // Integer

    static final public String PROP_RESULT_ORIGINAL_PAGE_COUNT = "OriginalPageCount"; // Integer

    static final public String PROP_RESULT_PAGE_NUMBER = PROP_PAGE_NUMBER; // Integer

    static final public String PROP_RESULT_SHAPE_NUMBER = PROP_SHAPE_NUMBER; // Integer

    static final public String PROP_RESULT_TEMP_INPUT_FILE = "TempInputFile"; // File

    static final public String PROP_RESULT_TEMP_INFO_FILENAME = PROP_INFO_FILENAME; // String

    static final public String PROP_RESULT_TEMP_INPUT_TYPE = PROP_INPUT_TYPE; // String

    static final public String PROP_RESULT_SINGLE_PAGE_JOBTYPE = "SinglePageJobType"; // String

    static final public String PROP_RESULT_ZIP_ARCHIVE = PROP_ZIP_ARCHIVE; // Boolean

    static final public String PROP_RESULT_CACHE_REMOTEURL = "CacheRemoteUrl"; // String

    static final public String PROP_RESULT_CONVERTER_COOKIE = PROP_CONVERTER_COOKIE; // String

    static final public String PROP_RESULT_ASYNC = PROP_ASYNC; // Boolean

    // -------------------------------------------------------------------------

    static public final String OX_RESCUEDOCUMENT_EXTENSION_APPENDIX = "_ox";

    static public final String OX_DOCUMENTCONVERTER_TEMPDIR_NAME = "oxdc.tmp";

    static public final String OX_DOCUMENTCONVERTER_LTSDIR_NAME = "oxdc.lts";

    // -------------------------------------------------------------------------

    final public static String JSON_KEY_CACHEHASH = "cacheHash";

    final public static String JSON_KEY_INPUTFILEHASH = "inputFileHash";

    final public static String JSON_KEY_LOCALE = "locale";

    final public static String JSON_KEY_ERRORCODE = "errorCode";

    final public static String JSON_KEY_ERRORDATA = "errorData";

    final public static String JSON_KEY_EXTENSION = "extension";

    final public static String JSON_KEY_JOBID = "jobId";

    final public static String JSON_KEY_MIMETYPE = "mimeType";

    final public static String JSON_KEY_PAGECOUNT = "pageCount";

    final public static String JSON_KEY_ORIGINALPAGECOUNT = "originalPageCount";

    final public static String JSON_KEY_PAGENUMBER = "pageNumber";

    final public static String JSON_KEY_SHAPENUMBER = "shapeNumber";

    final public static String JSON_KEY_INPUTFILE = "inputFile";

    final public static String JSON_KEY_INPUTTYPE = "inputType";

    final public static String JSON_KEY_INFOFILENAME = "infoFileName";

    final public static String JSON_KEY_SINGLEPAGEJOBTYPE = "singlePageJobType";

    final public static String JSON_KEY_ZIPARCHIVE = "zipArchive";

    final public static String JSON_KEY_CACHEREMOTEURL = "cacheRemoteUrl";

    final public static String JSON_KEY_CONVERTERCOOKIE = "converterCookie";

    final public static String JSON_KEY_ASYNC = "async";

    /**
     * @param <T>
     * @param map
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
    static public <T> T getValue(Map<String, ?> map, String key) {
        return (T) (((null != map) && (!StringUtils.isEmpty(key))) ? map.get(key) : null);
    }

    // -------------------------------------------------------------------------

    /**
     * @param resultProperties
     * @return
     */
    final public static JSONObject toJSONResult(final Map<String, Object> resultProperties) {
        final JSONObject jsonResult = new JSONObject();

        if (null != resultProperties) {
            try {
                Object curObj = null;

                if (null != (curObj = resultProperties.get(PROP_RESULT_CACHE_HASH))) {
                    jsonResult.put(JSON_KEY_CACHEHASH, curObj);
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_INPUTFILE_HASH))) {
                    jsonResult.put(JSON_KEY_INPUTFILEHASH, curObj);
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_LOCALE))) {
                    jsonResult.put(JSON_KEY_LOCALE, curObj);
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_ERROR_CODE))) {
                    jsonResult.put(JSON_KEY_ERRORCODE, ((Integer) curObj).intValue());
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_ERROR_DATA))) {
                    jsonResult.put(JSON_KEY_ERRORDATA, curObj);
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_EXTENSION))) {
                    jsonResult.put(JSON_KEY_EXTENSION, curObj);
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_JOBID))) {
                    jsonResult.put(JSON_KEY_JOBID, curObj);
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_MIME_TYPE))) {
                    jsonResult.put(JSON_KEY_MIMETYPE, curObj);
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_PAGE_COUNT))) {
                    jsonResult.put(JSON_KEY_PAGECOUNT, ((Integer) curObj).intValue());
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_ORIGINAL_PAGE_COUNT))) {
                    jsonResult.put(JSON_KEY_ORIGINALPAGECOUNT, ((Integer) curObj).intValue());
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_PAGE_NUMBER))) {
                    jsonResult.put(JSON_KEY_PAGENUMBER, ((Integer) curObj).intValue());
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_SHAPE_NUMBER))) {
                    jsonResult.put(JSON_KEY_SHAPENUMBER, ((Integer) curObj).intValue());
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_TEMP_INPUT_FILE))) {
                    jsonResult.put(JSON_KEY_INPUTFILE, ((File) curObj).getPath());
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_TEMP_INFO_FILENAME))) {
                    jsonResult.put(JSON_KEY_INFOFILENAME, curObj);
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_TEMP_INPUT_TYPE))) {
                    jsonResult.put(JSON_KEY_INPUTTYPE, curObj);
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_SINGLE_PAGE_JOBTYPE))) {
                    jsonResult.put(JSON_KEY_SINGLEPAGEJOBTYPE, curObj);
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_ZIP_ARCHIVE))) {
                    jsonResult.put(JSON_KEY_ZIPARCHIVE, ((Boolean) curObj).booleanValue());
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_CACHE_REMOTEURL))) {
                    jsonResult.put(JSON_KEY_CACHEREMOTEURL, curObj);
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_CONVERTER_COOKIE))) {
                    jsonResult.put(JSON_KEY_CONVERTERCOOKIE, curObj);
                }

                if (null != (curObj = resultProperties.get(PROP_RESULT_ASYNC))) {
                    jsonResult.put(JSON_KEY_ASYNC, ((Boolean) curObj).booleanValue());
                }
            } catch (JSONException e) {
                DocumentConverterManager.logExcp(e);
            }
        }

        return jsonResult;
    }

    /**
     * @param jsonObject
     * @return
     */
    final public static HashMap<String, Object> fromJSONResult(final JSONObject jsonObject) {
        final HashMap<String, Object> resultProperties = new HashMap<>(jsonObject.length());

        if (null != jsonObject) {
            String curStr = null;

            if (null != (curStr = jsonObject.optString(JSON_KEY_CACHEHASH))) {
                resultProperties.put(PROP_RESULT_CACHE_HASH, curStr);
            }

            if (null != (curStr = jsonObject.optString(JSON_KEY_INPUTFILEHASH))) {
                resultProperties.put(PROP_RESULT_INPUTFILE_HASH, curStr);
            }

            if (null != (curStr = jsonObject.optString(JSON_KEY_LOCALE))) {
                resultProperties.put(PROP_RESULT_LOCALE, curStr);
            }

            if (jsonObject.has(JSON_KEY_ERRORCODE)) {
                resultProperties.put(PROP_RESULT_ERROR_CODE, Integer.valueOf(jsonObject.optInt(JSON_KEY_ERRORCODE)));
            }

            if (null != (curStr = jsonObject.optString(JSON_KEY_ERRORDATA))) {
                resultProperties.put(PROP_RESULT_ERROR_DATA, curStr);
            }

            if (null != (curStr = jsonObject.optString(JSON_KEY_EXTENSION))) {
                resultProperties.put(PROP_RESULT_EXTENSION, curStr);
            }

            if (null != (curStr = jsonObject.optString(JSON_KEY_JOBID))) {
                resultProperties.put(PROP_RESULT_JOBID, curStr);
            }

            if (null != (curStr = jsonObject.optString(JSON_KEY_MIMETYPE))) {
                resultProperties.put(PROP_RESULT_MIME_TYPE, curStr);
            }

            if (jsonObject.has(JSON_KEY_PAGECOUNT)) {
                resultProperties.put(PROP_RESULT_PAGE_COUNT, Integer.valueOf(jsonObject.optInt(JSON_KEY_PAGECOUNT)));
            }

            if (jsonObject.has(JSON_KEY_ORIGINALPAGECOUNT)) {
                resultProperties.put(PROP_RESULT_ORIGINAL_PAGE_COUNT, Integer.valueOf(jsonObject.optInt(JSON_KEY_ORIGINALPAGECOUNT)));
            }

            if (jsonObject.has(JSON_KEY_PAGENUMBER)) {
                resultProperties.put(PROP_RESULT_PAGE_NUMBER, Integer.valueOf(jsonObject.optInt(JSON_KEY_PAGENUMBER)));
            }

            if (jsonObject.has(JSON_KEY_SHAPENUMBER)) {
                resultProperties.put(PROP_RESULT_SHAPE_NUMBER, Integer.valueOf(jsonObject.optInt(JSON_KEY_SHAPENUMBER)));
            }

            if (null != (curStr = jsonObject.optString(JSON_KEY_INPUTFILE))) {
                resultProperties.put(PROP_RESULT_TEMP_INPUT_FILE, new File(curStr));
            }

            if (null != (curStr = jsonObject.optString(JSON_KEY_INFOFILENAME))) {
                resultProperties.put(PROP_RESULT_TEMP_INFO_FILENAME, curStr);
            }

            if (null != (curStr = jsonObject.optString(JSON_KEY_INPUTTYPE))) {
                resultProperties.put(PROP_RESULT_TEMP_INPUT_TYPE, curStr);
            }

            if (null != (curStr = jsonObject.optString(JSON_KEY_SINGLEPAGEJOBTYPE))) {
                resultProperties.put(PROP_RESULT_SINGLE_PAGE_JOBTYPE, curStr);
            }

            if (jsonObject.has(JSON_KEY_ZIPARCHIVE)) {
                resultProperties.put(PROP_RESULT_ZIP_ARCHIVE, Boolean.valueOf(jsonObject.optBoolean(JSON_KEY_ZIPARCHIVE)));
            }

            if (null != (curStr = jsonObject.optString(JSON_KEY_CACHEREMOTEURL))) {
                resultProperties.put(PROP_RESULT_CACHE_REMOTEURL, curStr);
            }

            if (null != (curStr = jsonObject.optString(JSON_KEY_CONVERTERCOOKIE))) {
                resultProperties.put(PROP_RESULT_CONVERTER_COOKIE, curStr);
            }

            if (jsonObject.has(JSON_KEY_ASYNC)) {
                resultProperties.put(PROP_RESULT_ASYNC, Boolean.valueOf(jsonObject.optBoolean(JSON_KEY_ASYNC)));
            }
        }

        return resultProperties;
    }
}
