/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.impl;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.MutableWrapper;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;

/**
 * {@link ManagedClientHashJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.0
 */
public class ManagedClientHashJob extends ManagedClientJob {

    /**
     * Initializes a new {@link ManagedClientHashJob}.
     * @param manager
     */
    public ManagedClientHashJob(final ClientManager manager, final URL remoteUrl) {
        super(manager, remoteUrl);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ManagedRemoteJob#convert(java.lang.String, java.util.HashMap, java.util.HashMap)
     */
    @Override
    InputStream process(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        // always and only work on file based job properties here, since
        // we may need to call two subsequent conversions with job
        // properties, that are read more than once, which is problematic
        // in case of an InputStream based source content property
        final MutableWrapper<Boolean> deleteInputFile = new MutableWrapper<>(Boolean.FALSE);
        final HashMap<String, Object> fileBasedJobProperties = createFileBasedJobProperties(jobProperties, deleteInputFile);
        InputStream ret = null;

        if ((null != jobType) && (null != fileBasedJobProperties)) {
            final Boolean asyncObject = (Boolean) fileBasedJobProperties.get(Properties.PROP_ASYNC);
            final boolean isAsyncRequest = (null != asyncObject) && asyncObject.booleanValue();
            HashMap<String, Object> hashedJobProperties = getHashedJobProperties(jobType, fileBasedJobProperties, isAsyncRequest);
            String remoteCacheHash = null;
            boolean queryResultAvailability = false;
            final boolean trace = DocumentConverterManager.isLogTrace();
            long traceStartTimeMillis = 0;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                DocumentConverterManager.logTrace("DC client received request for " + (isAsyncRequest ? "async " : "") + "conversion", fileBasedJobProperties);
            }

            // try to get a valid result from a remotehash request first
            if (null != hashedJobProperties) {
                final String infoFileName = (String) hashedJobProperties.get(Properties.PROP_INFO_FILENAME);
                final Boolean queryAvailability = (Boolean) hashedJobProperties.get(Properties.PROP_QUERY_AVAILABILITY);

                queryResultAvailability = (null != queryAvailability) && queryAvailability.booleanValue();
                remoteCacheHash = (String) hashedJobProperties.get(Properties.PROP_REMOTE_CACHE_HASH);

                if (queryResultAvailability) {
                    if (trace) {
                        DocumentConverterManager.logTrace("DC client remotehash client query for asynchronous request initiated",
                            new LogData("remotehash", remoteCacheHash),
                            new LogData("filename", (null != infoFileName) ? infoFileName : "unkown"));
                    }

                    queryResultAvailability = super.queryAvailability(jobType, hashedJobProperties, resultProperties);

                    if (trace) {
                        DocumentConverterManager.logTrace("DC client remotehash client query for asynchronous request " + (queryResultAvailability ? "succeeded" : "failed"),
                            new LogData("remotehash", remoteCacheHash),
                            new LogData("filename", (null != infoFileName) ? infoFileName : "unkown"),
                            new LogData("exectime", Long.toString(System.currentTimeMillis() - traceStartTimeMillis) + "ms"));
                    }
                } else {
                    if (trace) {
                        DocumentConverterManager.logTrace("DC client remotehash client request initiated",
                            new LogData("remotehash", remoteCacheHash),
                            new LogData("filename", (null != infoFileName) ? infoFileName : "unkown"));
                    }

                    ret = super.process(jobType, hashedJobProperties, resultProperties);

                    if (trace) {
                        DocumentConverterManager.logTrace("DC client remotehash client request " + ((null != ret) ? "succeeded" : "failed"),
                            new LogData("remotehash", remoteCacheHash),
                            new LogData("filename", (null != infoFileName) ? infoFileName : "unkown"),
                            new LogData("exectime", Long.toString(System.currentTimeMillis() - traceStartTimeMillis) + "ms"));
                    }
                }
            }

            // if we got no result from remotehash request and
            // if we are not checking remote cache hash availability
            // only, perform the standard conversion request
            if ((null == ret) && !queryResultAvailability) {
                ret = super.process(jobType, fileBasedJobProperties, resultProperties);
            }
        }

        if ((null != fileBasedJobProperties) && deleteInputFile.get().booleanValue()) {
            FileUtils.deleteQuietly((File) fileBasedJobProperties.get(Properties.PROP_INPUT_FILE));
        }

        return ret;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param jobType
     * @param jobProperties
     * @return
     */
    protected HashMap<String, Object> getHashedJobProperties(@NonNull String jobType, @NonNull HashMap<String, Object> fileBasedJobProperties, boolean isAsyncRequest) {
        HashMap<String, Object> curJobProperties = null;
        final String remoteCacheHash = (String) fileBasedJobProperties.get(Properties.PROP_REMOTE_CACHE_HASH);

        if ((null != m_remoteUrl) && ((null == remoteCacheHash) || !remoteCacheHash.equals(Properties.CACHE_NO_CACHEHASH))) {
            final File jobInputFile = (File) fileBasedJobProperties.get(Properties.PROP_INPUT_FILE);
            StringBuilder pseudoHashBuilder = getPseudoHashBuilder(jobType, fileBasedJobProperties, jobInputFile);

            if ((null != pseudoHashBuilder) && (pseudoHashBuilder.length() > 0)) {
                // create a clone of job properties to remove some properties
                curJobProperties = new HashMap<>(fileBasedJobProperties.size());
                curJobProperties.putAll(fileBasedJobProperties);

                // completely remove old input sources
                curJobProperties.remove(Properties.PROP_INPUT_FILE);
                curJobProperties.remove(Properties.PROP_INPUT_STREAM);

                // a cache hash request returns always a result;
                // but in async case, we're not interested in
                // large result data at all;
                // so,   async flag and set method to
                // remote cache hash availability check
                if (isAsyncRequest) {
                    curJobProperties.remove(Properties.PROP_ASYNC);
                    curJobProperties.put(Properties.PROP_QUERY_AVAILABILITY, Boolean.TRUE);
                }

                // set pseudo hash to use as pseudo input source
                curJobProperties.put(Properties.PROP_REMOTE_CACHE_HASH, pseudoHashBuilder.toString());
            }
        }

        return curJobProperties;
    }

    /**
     * @param jobType
     * @param jobProperties
     * @param jobInputFile
     * @return
     */
    StringBuilder getPseudoHashBuilder(@NonNull String jobType, @NonNull HashMap<String, Object> jobProperties, @NonNull File jobInputFile) {
        StringBuilder pseudoHashBuilder = null;

        switch (jobType) {
            case "pdf":
            case "pdfa":
            case "odf":
            case "ooxml": {
                final String pageRange = (String) jobProperties.get(Properties.PROP_PAGE_RANGE);

                // default input file hash including job type
                pseudoHashBuilder = getDefaultPseudoHashBuilder(jobType, jobProperties, jobInputFile);

                // page range
                pseudoHashBuilder.append('#').append(!StringUtils.isEmpty(pageRange) ? pageRange : "1-99999");

                break;
            }

            case "graphic": {
                // default input file hash
                pseudoHashBuilder = getDefaultPseudoHashBuilder("jpg", jobProperties, jobInputFile);
                break;
            }

            case "preview": {
                // Graphic scaleType
                final Boolean autoscale = (Boolean) jobProperties.get(Properties.PROP_AUTOSCALE);
                final String scaleType = (String) jobProperties.get(Properties.PROP_IMAGE_SCALE_TYPE);
                final String pageRange = (String) jobProperties.get(Properties.PROP_PAGE_RANGE);
                Integer width = (Integer) jobProperties.get(Properties.PROP_PIXEL_WIDTH);
                Integer height = (Integer) jobProperties.get(Properties.PROP_PIXEL_HEIGHT);

                String resultMimeType = (String) jobProperties.get(Properties.PROP_MIME_TYPE);

                resultMimeType = ((null == resultMimeType) || (resultMimeType.length() <= 0)) ? "image/jpeg" : resultMimeType.toLowerCase();

                // default input file hash including job type;
                pseudoHashBuilder = getDefaultPseudoHashBuilder("p2g", jobProperties, jobInputFile);

                // job type and target image mime type
                pseudoHashBuilder.append("image/jpeg".equals(resultMimeType) ? "jpg" : "png");

                // autoscale
                if ((null != autoscale) && autoscale.booleanValue()) {
                    pseudoHashBuilder.append('a');
                }

                // scale type
                if (!StringUtils.isEmpty(scaleType)) {
                    pseudoHashBuilder.append(scaleType.substring(0, 3));
                }

                // pageRange
                if (null != pageRange) {
                    pseudoHashBuilder.append('#').append(pageRange);
                }

                // pixel width and height
                pseudoHashBuilder.append('@').append((null != width) ? width.intValue() : -1).append('x').append((null != height) ? height.intValue() : -1);

                break;
            }

            default: {
                break;
            }
        }

        return pseudoHashBuilder;
    }

    /**
     * @param jobType
     * @param jobProperties
     * @param jobInputFile
     * @return
     */
    StringBuilder getDefaultPseudoHashBuilder(@NonNull String jobType, @NonNull HashMap<String, Object> jobProperties, @NonNull File jobInputFile) {
        return DocumentConverterManager.getFileHashBuilder(
            jobInputFile,
            (String) jobProperties.get(Properties.PROP_INPUT_URL),
            (String) jobProperties.get(Properties.PROP_LOCALE)).append(jobType);
    }

    /**
     * @param jobProperties
     * @param deleteInputFile
     * @return
     */
    HashMap<String, Object> createFileBasedJobProperties(HashMap<String, Object> jobProperties, MutableWrapper<Boolean> deleteInputFile) {
        final File jobInputFile = m_clientManager.getJobInputFile(jobProperties, deleteInputFile);
        HashMap<String, Object> fileBasedJobProperties = null;

        // ensure, that we always have a file based job property set to work on
        if (null != jobInputFile) {
            fileBasedJobProperties = new HashMap<>(jobProperties.size());
            fileBasedJobProperties.putAll(jobProperties);
            fileBasedJobProperties.remove(Properties.PROP_INPUT_STREAM);
            fileBasedJobProperties.put(Properties.PROP_INPUT_FILE, jobInputFile);
        }

        return fileBasedJobProperties;
    }
}
