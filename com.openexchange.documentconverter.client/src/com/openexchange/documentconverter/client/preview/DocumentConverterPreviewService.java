/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.preview;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.conversion.Data;
import com.openexchange.conversion.DataProperties;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.client.impl.ClientManager;
import com.openexchange.documentconverter.client.preview.PreviewResult.PreviewType;
import com.openexchange.exception.OXException;
import com.openexchange.imagetransformation.ImageTransformationService;
import com.openexchange.imagetransformation.ScaleType;
import com.openexchange.preview.PreviewDocument;
import com.openexchange.preview.PreviewExceptionCodes;
import com.openexchange.preview.PreviewOutput;
import com.openexchange.preview.PreviewPolicy;
import com.openexchange.preview.Quality;
import com.openexchange.preview.RemoteInternalPreviewService;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.user.User;

/**
 * {@link DocumentConverterPreviewService}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @author <a href="mailto:oliver.specht@open-xchange.com">Oliver Specht</a>
 * @author <a href="mailto:sven.jacobi@open-xchange.com">Sven Jacobi</a>
 */
public class DocumentConverterPreviewService implements RemoteInternalPreviewService {

    /**
     * Initializes a new {@link DocumentConverterPreviewService}.
     *
     * @param serviceLookup
     */
    public DocumentConverterPreviewService(final ServiceLookup serviceLookup) {
        super();
        m_serviceLookup = serviceLookup;
    }

    @Override
    public boolean isSupportedFor(Session session) throws OXException {
        CapabilityService capabilityService = m_serviceLookup.getOptionalService(CapabilityService.class);
        if (null == capabilityService) {
            // Cannot check since service is absent
            return false;
        }

        return capabilityService.getCapabilities(session).contains("document_preview");
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.preview.RemoteInternalPreviewService#getTimeToWaitMillis()
     */
    @Override
    public long getTimeToWaitMillis() {
        // Signal to wait at max. 10 seconds for preview document retrieval
        return 10000;
    }

    /* (non-Javadoc)
     * @see com.openexchange.preview.RemoteInternalPreviewService#triggerGetPreviewFor(java.lang.String, com.openexchange.preview.PreviewOutput, com.openexchange.session.Session, int)
     */
    @Override
    @Deprecated
    public void triggerGetPreviewFor(String arg, PreviewOutput output, Session session, int pages) throws OXException {
        // nothing to be done here; Parameter set is not supported anymore
    }

    /* (non-Javadoc)
     * @see com.openexchange.preview.RemoteInternalPreviewService#triggerGetPreviewFor(com.openexchange.conversion.Data, com.openexchange.preview.PreviewOutput, com.openexchange.session.Session, int)
     */
    @Override
    public void triggerGetPreviewFor(Data<InputStream> documentData, PreviewOutput output, Session session, int pages) throws OXException {
        final IDocumentConverter docConverter = m_serviceLookup.getService(IDocumentConverter.class);

        if ((null != docConverter) && (null != documentData)) {
            final DataProperties dataProperties = documentData.getDataProperties();

            if (null != dataProperties) {
                final File documentFile = createTempFile(documentData);

                if (null != documentFile) {
                    String fileName = dataProperties.get(DataProperties.PROPERTY_NAME);
                    final String fileMimeType = dataProperties.get(DataProperties.PROPERTY_CONTENT_TYPE);
                    final String fileInputType = getInputType(fileMimeType, fileName, documentFile);


                    if (!"pdf".equals(fileInputType)) {
                        final String previewLanguage = detectPreviewLanguage(dataProperties.get("PreviewLanguage"), session);
                        final HashMap<String, Object> jobProperties = new HashMap<>(12);
                        final HashMap<String, Object> resultProperties = new HashMap<>(8);

                        jobProperties.put(Properties.PROP_INPUT_FILE, documentFile);
                        jobProperties.put(Properties.PROP_PRIORITY, JobPriority.BACKGROUND);
                        jobProperties.put(Properties.PROP_ASYNC, Boolean.TRUE);
                        jobProperties.put(Properties.PROP_INFO_FILENAME, fileName);

                        // add locale, if available
                        if (null != previewLanguage) {
                            jobProperties.put(Properties.PROP_LOCALE, previewLanguage);
                        }

                        // perform an async request for PDF conversion
                        if (DocumentConverterManager.isLogTrace()) {
                            DocumentConverterManager.logTrace("DC OfficePreviewService performs asynchronous PDF conversion for document: " + ((null != fileName) ? fileName : ""));
                        }

                        docConverter.convert("pdf", jobProperties, resultProperties);
                    } else {
                        if (DocumentConverterManager.isLogTrace()) {
                            DocumentConverterManager.logTrace("DC OfficePreviewService received asynchronous PDF conversion request for PDF document => no need to convert: " + ((null != fileName) ? fileName : ""));
                        }
                    }

                    FileUtils.deleteQuietly(documentFile);
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.preview.PreviewService#detectDocumentType(java.io.InputStream)
     */
    @Override
    public String detectDocumentType(InputStream inputStream) {
        return null;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.preview.PreviewService#getPreviewFor(java.lang.String, com.openexchange.preview.PreviewOutput,
     * com.openexchange.session.Session, int)
     */
    @Override
    public PreviewDocument getPreviewFor(String arg, PreviewOutput output, Session session, int pages) {
        return new DocumentConverterPreviewDocument(null, null, null, Boolean.FALSE);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.preview.PreviewService#getPreviewFor(com.openexchange.conversion.Data, com.openexchange.preview.PreviewOutput,
     * com.openexchange.session.Session, int)
     */
    @Override
    public PreviewDocument getPreviewFor(Data<InputStream> documentData, PreviewOutput output, Session session, int pages) throws OXException {
        return getPreview(documentData, output, session, pages, false, false);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.preview.InternalPreviewService#getCachedPreviewFor(com.openexchange.conversion.Data,
     * com.openexchange.preview.PreviewOutput, com.openexchange.session.Session, int)
     */
    @Override
    public PreviewDocument getCachedPreviewFor(Data<InputStream> documentData, PreviewOutput output, Session session, int pages) throws OXException {
        return getPreview(documentData, output, session, pages, true, false);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.preview.InternalPreviewService#getPreviewPolicies()
     */
    @Override
    public List<PreviewPolicy> getPreviewPolicies() {
        return POLICIES;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.preview.InternalPreviewService#canDetectContentType()
     */
    @Override
    public boolean canDetectContentType() {
        return false;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param documentData
     * @param output
     * @param session
     * @param pages
     * @param cacheOnly
     * @return
     */
    public PreviewDocument getPreview(Data<InputStream> documentData, PreviewOutput output, Session session, int pages, boolean cacheOnly, boolean async) throws OXException {
        PreviewDocument previewDocument = null;

        if ((null != documentData) && (null != output)) {
            File tmpSourceFile = null;

            try {
                final IDocumentConverter docConverter = m_serviceLookup.getService(IDocumentConverter.class);

                if ((null != docConverter) && (!cacheOnly || ClientManager.get().getClientConfig().PREVIEW_ENABLE_CACHE_LOOKUP) && (null != (tmpSourceFile = createTempFile(documentData)))) {
                    previewDocument = getPreviewDocument(docConverter, tmpSourceFile, documentData, output, session, cacheOnly, async);
                }
            } finally {
                FileUtils.deleteQuietly(tmpSourceFile);
            }
        }

        return previewDocument;
    }

    /**
     * @param documentBuffer
     * @param documentData
     * @param output
     * @return The object containing the preview result or null in case of an error
     */
    public PreviewDocument getPreviewDocument(IDocumentConverter docConverter, File documentFile, Data<InputStream> documentData, PreviewOutput output, Session session, boolean cacheOnly, boolean async) throws OXException {
        DocumentConverterPreviewDocument previewDocument = null;
        final DataProperties dataProperties = documentData.getDataProperties();
        final String previewLanguage = detectPreviewLanguage(dataProperties.get("PreviewLanguage"), session);
        final int previewWidth = NumberUtils.toInt(dataProperties.get("PreviewWidth"), 0);
        final int previewHeight = NumberUtils.toInt(dataProperties.get("PreviewHeight"), 0);
        final String scaleTypeString = dataProperties.get("PreviewScaleType");
        final ScaleType scaleType = (null != scaleTypeString) ? (scaleTypeString.equalsIgnoreCase("cover") ? ScaleType.COVER : ScaleType.CONTAIN) : ScaleType.CONTAIN;
        final String scaleTypeProperty = ((ScaleType.COVER == scaleType) ? "cover" : "contain");
        final HashMap<String, Object> jobProperties = new HashMap<>(10);
        final HashMap<String, Object> resultProperties = new HashMap<>(8);
        final Map<String, String> metaData = new HashMap<>(4);
        final boolean thumbnailPreview = (previewHeight != 0);
        final String fileName = dataProperties.get(DataProperties.PROPERTY_NAME);
        final String fileMimeType = dataProperties.get(DataProperties.PROPERTY_CONTENT_TYPE);
        final String fileInputType = getInputType(fileMimeType, fileName, documentFile);
        final boolean hideChanges = true;
        final boolean hideComments = true;
        InputStream resultInputStm = null;
        byte[] resultBuffer = null;

        // if set, just try to get a fast cached result and don't
        // trigger a real conversion, even if no cached result is available;
        // nevertheless, fast previews and similar are treated as cached previews
        if (cacheOnly) {
            jobProperties.put(Properties.PROP_CACHE_ONLY, Boolean.TRUE);
        }

        // async preview trigger requests are always scheduled with BACKGROUND priority
        jobProperties.put(Properties.PROP_PRIORITY, async ? JobPriority.BACKGROUND : (thumbnailPreview ? JobPriority.LOW : JobPriority.MEDIUM));

        jobProperties.put(Properties.PROP_PAGE_RANGE, "1");

        if (null != previewLanguage) {
            jobProperties.put(Properties.PROP_LOCALE, previewLanguage);
        }

        if (hideChanges) {
            jobProperties.put(Properties.PROP_HIDE_CHANGES, Boolean.TRUE);
        }

        if (hideComments) {
            jobProperties.put(Properties.PROP_HIDE_COMMENTS, Boolean.TRUE);
        }

        /* TODO (KA): set InputUrl property, if known later
        if (null != inputURL) {
            jobProperties.put(Properties.PROP_INPUT_URL, inputURL);
        }
        */

        // just log the filename of the request in error case
        if (null != fileName) {
            jobProperties.put(Properties.PROP_INFO_FILENAME, fileName);
        }

        if (PreviewOutput.IMAGE == output) {
            final String jpegMimeType = "image/jpeg";

            jobProperties.put(Properties.PROP_MIME_TYPE, jpegMimeType);

            if (previewWidth > 0) {
                jobProperties.put(Properties.PROP_PIXEL_WIDTH, Integer.valueOf(previewWidth));
            }
            if (previewHeight > 0) {
                jobProperties.put(Properties.PROP_PIXEL_HEIGHT, Integer.valueOf(previewHeight));
            }

            jobProperties.put(Properties.PROP_IMAGE_SCALE_TYPE, scaleTypeProperty);

            if (thumbnailPreview) {
                // try to reuse already existing previews from zipArchive and/or the compound object
                PreviewResult fastPreviewResult = getFastPreview(documentFile, fileMimeType);

                if (null != fastPreviewResult) {
                    // we have some kind of preview graphics;
                    // unsupported mimetypes (e.g. emf/pnf/wmf) need to
                    // be converted to JPEG images using the
                    // document converter first
                    if (!supportedTypes.contains(fastPreviewResult.getPreviewMimeType())) {
                        final String fastPreviewInputType = getInputType(fastPreviewResult.getPreviewMimeType(), null, null);

                        try (final ByteArrayInputStream grfInputStm = new ByteArrayInputStream(fastPreviewResult.getPreviewBuffer())) {
                            jobProperties.put(Properties.PROP_INPUT_STREAM, grfInputStm);
                            jobProperties.put(Properties.PROP_INPUT_TYPE, fastPreviewInputType);

                            if (null != fileName) {
                                jobProperties.put(Properties.PROP_INFO_FILENAME, fileName + ".thumbnail");
                            }

                            // this is a user request in case of a synchronous request
                            if (!async) {
                                jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);
                            }

                            resultProperties.clear();

                            try (final InputStream grfResultInputStm = docConverter.convert("graphic", jobProperties, resultProperties)) {
                                if (null != grfResultInputStm) {
                                    // try to get direct access to the result buffer to avoid an array copy;
                                    // if result buffer is null, retrieve the result buffer from the result stream
                                    if (null == (resultBuffer = (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER))) {
                                        resultBuffer = IOUtils.toByteArray(grfResultInputStm);
                                    }

                                    // exchange the existing fast image PreviewResult with
                                    // the result of the graphic conversion
                                    if (null != resultBuffer) {
                                        fastPreviewResult = new PreviewResult(jpegMimeType, resultBuffer);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            handleExcp(e);
                        }
                    }

                    // images with the proper mime tye are finally
                    // scaled to the proper thumbnail destination size
                    if (("image/jpeg".equals(fastPreviewResult.m_mimeType) || "image/png".equals(fastPreviewResult.m_mimeType)) &&
                        ((previewWidth > 0) || (previewHeight > 0))) {

                        ImageTransformationService imageScalingService = m_serviceLookup.getService(ImageTransformationService.class);

                        if (null != imageScalingService) {
                            try {
                                resultBuffer = imageScalingService.transfom(fastPreviewResult.getPreviewBuffer()).scale(
                                        previewWidth,
                                        previewHeight,
                                        scaleType).getBytes("image/jpeg");
                            } catch (Exception e) {
                                handleExcp(e);
                            }
                        }
                    }

                    metaData.put("content-type", jpegMimeType);
                    metaData.put("resourcename", "image.jpg");

                    DocumentConverterManager.logTrace("DC OfficePreviewService fulfills thumbnail request from thumbnail contained in document: " + ((null != fileName) ? fileName : ""));
                    previewDocument = new DocumentConverterPreviewDocument(metaData, null, resultBuffer, Boolean.FALSE);
                }
            }

            // no preview could be created so far => we have to use the DocumentConverter
            if (null == previewDocument) {
                jobProperties.put(Properties.PROP_INPUT_FILE, documentFile);
                jobProperties.put(Properties.PROP_INPUT_TYPE, fileInputType);

                // is set to true, the preview request is performed asynchronously and
                if (async) {
                    jobProperties.put(Properties.PROP_ASYNC, Boolean.TRUE);
                } else {
                    // this is a user request in case of a synchronous request
                    jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);
                }

                resultProperties.clear();

                try {
                    DocumentConverterManager.logTrace("DC OfficePreviewService converts document to create thumbnail: " + ((null != fileName) ? fileName : ""));

                    if (null != (resultInputStm = docConverter.convert("preview", jobProperties, resultProperties))) {
                        // try to get direct access to the result buffer to avoid an array copy;
                        // if result buffer is null, retrieve the result buffer from the result stream
                        if (null == (resultBuffer = (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER))) {
                            resultBuffer = IOUtils.toByteArray(resultInputStm);
                        }
                    }
                } catch (Exception e) {
                    handleExcp(e);
                } finally {
                    DocumentConverterManager.close(resultInputStm);
                    resultInputStm = null;
                }

                if ((null == resultBuffer) && thumbnailPreview) {
                    // create password protection icon result for thumbnails, if PASSWORD_PROTECTED result flag is set
                    if (JobError.PASSWORD == JobErrorEx.fromResultProperties(resultProperties).getJobError()) {
                        String resourceName = "pw_text.jpg";

                        if (null != fileMimeType) {
                            if (fileMimeType.contains("spreadsheet") || fileMimeType.contains("excel") || fileMimeType.contains("calc")) {
                                resourceName = "pw_spreadsheet.jpg";
                            } else if (fileMimeType.contains("presentation") || fileMimeType.contains("draw") || fileMimeType.contains("powerpoint") || fileMimeType.contains("impress")) {
                                resourceName = "pw_presentation.jpg";
                            }
                        }

                        // get PW protected document icon buffer from resource
                        resultBuffer = getResourceBuffer("icons/" + resourceName);
                    }
                }

                metaData.put("content-type", jpegMimeType);
                metaData.put("resourcename", "image.jpg");

                previewDocument = new DocumentConverterPreviewDocument(metaData, null, resultBuffer, Boolean.FALSE);
            }
        } else {
            final List<String> stringList = new ArrayList<>();

            jobProperties.put(Properties.PROP_INPUT_FILE, documentFile);
            jobProperties.put(Properties.PROP_INPUT_TYPE, fileInputType);

            // is set to true, the preview request is performed asynchronously and
            if (async) {
                jobProperties.put(Properties.PROP_ASYNC, Boolean.TRUE);
            } else {
                jobProperties.put(Properties.PROP_USER_REQUEST, Boolean.TRUE);
            }

            resultProperties.clear();

            try {
                resultInputStm = docConverter.convert("htmlpage", jobProperties, resultProperties);
            } catch (Exception e) {
                handleExcp(e);
            } finally {
                DocumentConverterManager.close(resultInputStm);
                resultInputStm = null;
            }

            metaData.put("content-type", "text/html");
            metaData.put("resourcename", "document.html");

            previewDocument = new DocumentConverterPreviewDocument(metaData, stringList, null, Boolean.FALSE);
        }

        return previewDocument;
    }

    /**
     * @param sourceStream
     * @param thumbnailName
     * @return
     */
    private static byte[] getZipThumbnailBuffer(@NonNull File inputFile, @NonNull String thumbnailName) throws OXException {
        byte[] resultBuffer = null;
        InputStream inputStm = null;
        ZipInputStream zipInputStm = null;

        try {
            inputStm = new BufferedInputStream(FileUtils.openInputStream(inputFile));
            zipInputStm = new ZipInputStream(inputStm);

            for (ZipEntry zipEntry = zipInputStm.getNextEntry(); zipEntry != null; zipEntry = zipInputStm.getNextEntry()) {
                if (zipEntry.getName().equals(thumbnailName)) {
                    try {
                        resultBuffer = IOUtils.toByteArray(zipInputStm);
                    } catch (IOException e) {
                        handleExcp(e);
                    }

                    break;
                }
            }
        } catch (@SuppressWarnings("unused") IOException e) {
            // ok, this may happen with some formats as a try,
            // but shouldn't give any output in the log file
        } finally {
            DocumentConverterManager.close(zipInputStm);
            DocumentConverterManager.close(inputStm);
        }

        return resultBuffer;
    }

    /**
     * @param filename
     * @return
     */
    private static byte[] getResourceBuffer(@NonNull String filename) throws OXException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        byte[] resourceBuffer = null;

        if (null == loader) {
            loader = DocumentConverterPreviewService.class.getClassLoader();
        }

        final URL url = loader.getResource(filename);

        if (null != url) {
            URLConnection connection = null;
            InputStream resourceInputStream = null;

            try {
                connection = url.openConnection();

                if (null != connection) {
                    connection.connect();

                    if (null != (resourceInputStream = connection.getInputStream())) {
                        resourceBuffer = IOUtils.toByteArray(resourceInputStream);
                    }
                }
            } catch (IOException e) {
                handleExcp(e);
            } finally {
                DocumentConverterManager.close(resourceInputStream);
            }
        }

        return resourceBuffer;
    }

    /**
     * @param sourceStream
     * @param m_mimeType
     * @return
     */
    private static PreviewResult getFastPreview(@NonNull File inputFile, String mimeType) throws OXException {
        PreviewResult fastPreviewResult = null;
        PreviewResult.PreviewType fastPreviewType = fastPreviewTypes.get(mimeType);

        if (fastPreviewType != null) {
            switch (fastPreviewType) {
            case OOXML: {
                final byte[] thumbnailBuffer = getZipThumbnailBuffer(inputFile, "docProps/thumbnail.jpeg");

                if (null != thumbnailBuffer) {
                    fastPreviewResult = new PreviewResult("image/jpeg", thumbnailBuffer);
                }

                break;
            }

            /* DOCS-4079: do not use document internal preview thumbnail for ODF document type anymore
            case ODF: {
                final byte[] thumbnailBuffer = getZipThumbnailBuffer(inputFile, "Thumbnails/thumbnail.png");

                if (null != thumbnailBuffer) {
                    fastPreviewResult = new PreviewResult("image/png", thumbnailBuffer);
                }

                break;
            }
            */

            case NONE:
            default:
                break;

            }
        }

        return fastPreviewResult;
    }

    /**
     * @param e
     * @throws OXException
     */
    final protected static void handleExcp(Exception e) throws OXException {
        if (null != e) {
            if (e instanceof IOException) {
                throw PreviewExceptionCodes.IO_ERROR.create(e, e.getMessage());
            } else if (e instanceof RuntimeException) {
                throw PreviewExceptionCodes.ERROR.create(e, e.getMessage());
            } else {
                DocumentConverterManager.logExcp(e);
            }
        }
    }

    // checked: inputStm is always closed at end of method
    final protected static File createTempFile(@NonNull Data<InputStream> documentData) throws OXException {
        File ret = null;

        try (final InputStream inputStm = documentData.getData()) {
            if ((null != inputStm) && (null != (ret = ClientManager.get().createTempFile("oxpreview")))) {
                FileUtils.copyInputStreamToFile(inputStm, ret);
            }
        } catch (IOException e) {
            FileUtils.deleteQuietly(ret);
            ret = null;

            handleExcp(e);
        }

        return ret;
    }

    /**
     * @param mimeType
     * @return
     */
    final protected static String getInputType(String fileMimeType, String fileName, File file) {
        final String extension = (null != fileName) ? FilenameUtils.getExtension(fileName).toLowerCase().trim() : "";
        String ret = null;

        // explicitly check for input type %PDF-x.x
        if (null != file) {
            try (final InputStream inputStm = FileUtils.openInputStream(file)) {
                if (null != inputStm) {
                    final byte[] buffer = new byte[256];

                    if ((inputStm.read(buffer) > 0) && new String(buffer, "UTF-8").trim().toLowerCase().startsWith("%pdf-")) {
                        ret = "pdf";
                    }
                }
            } catch (IOException e) {
                DocumentConverterManager.logExcp(e);
            }
        }

        // other types are checked based on the given mimetype and/or extension
        if (null == ret) {
            if ("image/png".equals(fileMimeType) || "pdf".equals(extension)) {
                ret = "png";
            } else if ("image/jpeg".equals(fileMimeType) || "jpg".equals(extension) || "jpeg".equals(extension)) {
                ret = "jpg";
            } else if ("image/svg+xml".equals(fileMimeType) || "svg".equals(extension)) {
                ret = "svg";
            } else if (extension.length() > 0) {
                ret = extension;
            }
        }

        return ret;
    }

    // - Members ---------------------------------------------------------------

    final private ServiceLookup m_serviceLookup;

    final private static List<PreviewPolicy> POLICIES = new ArrayList<>() {

        final private static long serialVersionUID = 1L;

        {
            add(new PreviewPolicy("text/plain", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("text/rtf", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("text/rtf", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("text/csv", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("text/csv", PreviewOutput.IMAGE, Quality.GOOD));

            add(new PreviewPolicy("application/rtf", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/rtf", PreviewOutput.IMAGE, Quality.GOOD));

            add(new PreviewPolicy("application/pdf", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/pdf", PreviewOutput.IMAGE, Quality.GOOD));

            add(new PreviewPolicy("application/msword", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/msword", PreviewOutput.IMAGE, Quality.GOOD));

            add(new PreviewPolicy("application/excel", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/excel", PreviewOutput.IMAGE, Quality.GOOD));

            add(new PreviewPolicy("application/powerpoint", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/powerpoint", PreviewOutput.IMAGE, Quality.GOOD));

            add(new PreviewPolicy("application/x-tika-ooxml", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/x-tika-ooxml", PreviewOutput.IMAGE, Quality.GOOD));

            add(new PreviewPolicy("application/vnd.ms-word.document", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-word.document", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-word.document.macroenabled.12", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-word.document.macroenabled.12", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-word.template", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-word.template", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-word.template.macroenabled.12", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-word.template.macroenabled.12", PreviewOutput.IMAGE, Quality.GOOD));

            add(new PreviewPolicy("application/vnd.ms-excel", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.sheet", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.sheet", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.sheet.macroenabled.12", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.sheet.macroenabled.12", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.sheet.binary", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.sheet.binary", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.sheet.binary.macroenabled.12", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.sheet.binary.macroenabled.12", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.template", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.template", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.template.macroenabled.12", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.template.macroenabled.12", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.addin", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.addin", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.addin.macroenabled.12", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-excel.addin.macroenabled.12", PreviewOutput.IMAGE, Quality.GOOD));

            add(new PreviewPolicy("application/vnd.ms-powerpoint", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.presentation", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.presentation", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.presentation.macroenabled.12", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.presentation.macroenabled.12", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.template", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.template", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.template.macroenabled.12", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.template.macroenabled.12", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.slideshow.", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.slideshow.", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.slideshow.macroenabled.12", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.slideshow.macroenabled.12", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.addin", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.addin", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.addin.macroenabled.12", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.ms-powerpoint.addin.macroenabled.12", PreviewOutput.IMAGE, Quality.GOOD));

            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.wordprocessingml.document", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.wordprocessingml.document", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.wordprocessingml.template", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.wordprocessingml.template", PreviewOutput.IMAGE,Quality.GOOD));

            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.spreadsheetml.template", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.spreadsheetml.template", PreviewOutput.IMAGE, Quality.GOOD));

            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.presentationml.presentation", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.presentationml.presentation", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.presentationml.template", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.presentationml.template", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.presentationml.slideshow", PreviewOutput.HTML, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.openxmlformats-officedocument.presentationml.slideshow", PreviewOutput.IMAGE, Quality.GOOD));

            add(new PreviewPolicy("application/vnd.oasis.opendocument.text", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.oasis.opendocument.text-template", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.oasis.opendocument.spreadsheet", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.oasis.opendocument.spreadsheet-template", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.oasis.opendocument.presentation", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.oasis.opendocument.presentation-template", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.oasis.opendocument.graphics", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.oasis.opendocument.graphics-template", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.oasis.opendocument.formula", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.oasis.opendocument.image", PreviewOutput.IMAGE, Quality.GOOD));
            add(new PreviewPolicy("application/vnd.oasis.opendocument.text-master", PreviewOutput.IMAGE, Quality.GOOD));
        }
    };

    /**
     * compoundBinaryMimeTypes are mimeTypes for binary documents containing OLE2 Properties which then might include a thumbnail within the
     * summaryInformation property set (wmf/emf or bmp format)
     */
    final private static HashMap<String, PreviewResult.PreviewType> fastPreviewTypes = new HashMap<>() {

        final private static long serialVersionUID = 1L;

        {
            put("application/vnd.ms-word.document", PreviewType.OOXML);
            put("application/vnd.ms-word.document.macroenabled.12", PreviewType.OOXML);
            put("application/vnd.ms-word.template", PreviewType.OOXML);
            put("application/vnd.ms-word.template.macroenabled.12", PreviewType.OOXML);

            put("application/vnd.ms-excel.sheet.binary", PreviewType.OOXML);
            put("application/vnd.ms-excel.sheet.binary.macroenabled.12", PreviewType.OOXML);
            put("application/vnd.ms-excel.sheet", PreviewType.OOXML);
            put("application/vnd.ms-excel.sheet.macroenabled.12", PreviewType.OOXML);
            put("application/vnd.ms-excel.template", PreviewType.OOXML);
            put("application/vnd.ms-excel.template.macroenabled.12", PreviewType.OOXML);
            put("application/vnd.ms-excel.addin", PreviewType.OOXML);
            put("application/vnd.ms-excel.addin.macroenabled.12", PreviewType.OOXML);

            put("application/vnd.ms-powerpoint.presentation", PreviewType.OOXML);
            put("application/vnd.ms-powerpoint.presentation.macroenabled.12", PreviewType.OOXML);
            put("application/vnd.ms-powerpoint.template", PreviewType.OOXML);
            put("application/vnd.ms-powerpoint.template.macroenabled.12", PreviewType.OOXML);
            put("application/vnd.ms-powerpoint.slideshow", PreviewType.OOXML);
            put("application/vnd.ms-powerpoint.slideshow.macroenabled.12", PreviewType.OOXML);
            put("application/vnd.ms-powerpoint.addin", PreviewType.OOXML);
            put("application/vnd.ms-powerpoint.addin.macroenabled.12", PreviewType.OOXML);

            put("application/vnd.openxmlformats-officedocument.presentationml.presentation", PreviewType.OOXML);
            put("application/vnd.openxmlformats-officedocument.presentationml.template", PreviewType.OOXML);
            put("application/vnd.openxmlformats-officedocument.presentationml.slideshow", PreviewType.OOXML);
            put("application/vnd.openxmlformats-officedocument.wordprocessingml.document", PreviewType.OOXML);
            put("application/vnd.openxmlformats-officedocument.wordprocessingml.template", PreviewType.OOXML);
            put("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", PreviewType.OOXML);
            put("application/vnd.openxmlformats-officedocument.spreadsheetml.template", PreviewType.OOXML);

            put("application/vnd.oasis.opendocument.text", PreviewType.ODF);
            put("application/vnd.oasis.opendocument.text-template", PreviewType.ODF);
            put("application/vnd.oasis.opendocument.spreadsheet", PreviewType.ODF);
            put("application/vnd.oasis.opendocument.spreadsheet-template", PreviewType.ODF);
            put("application/vnd.oasis.opendocument.presentation", PreviewType.ODF);
            put("application/vnd.oasis.opendocument.presentation-template", PreviewType.ODF);
            put("application/vnd.oasis.opendocument.graphics", PreviewType.ODF);
            put("application/vnd.oasis.opendocument.graphics-template", PreviewType.ODF);
            put("application/vnd.oasis.opendocument.formula", PreviewType.ODF);
            put("application/vnd.oasis.opendocument.image", PreviewType.ODF);
            put("application/vnd.oasis.opendocument.text-master", PreviewType.ODF);
        }
    };

    /**
     *
     */
    final private static HashSet<String> supportedTypes = new HashSet<>() {

        private static final long serialVersionUID = 1L;

        {
            add("image/jpeg");
            add("image/svg+xml");
        }
    };

    /**
     * @param language
     * @param session
     * @return
     */
    final private static String detectPreviewLanguage(String language, Session session) {
        String previewLanguage = language;

           // detect language from ServerSessionUser, if available
        if ((null == previewLanguage) && (session instanceof ServerSession)) {
            final User sessionUser = ((ServerSession) session).getUser();

            if (null != sessionUser) {
                previewLanguage = sessionUser.getPreferredLanguage();
            }
        }

        return previewLanguage;
    }
}
