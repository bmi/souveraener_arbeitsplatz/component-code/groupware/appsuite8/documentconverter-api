/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.client.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import com.openexchange.config.ConfigurationService;
import com.openexchange.config.Interests;
import com.openexchange.config.Reloadable;
import com.openexchange.documentconverter.AsyncExecutor;
import com.openexchange.documentconverter.DocumentConverterClientMetrics;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.PDFExtractor;
import com.openexchange.documentconverter.SingletonService;


/**
 * {@link ClientManager}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.0
 */
@SingletonService
public class ClientManager extends DocumentConverterManager implements IDocumentConverter, Reloadable {

    final private static String DC_CLIENT_PDF_EXTRACTOR_LOG_PREFIX = "DC client PDFExtractor";

    final private static int ASYNC_QUEUE_LENTGH = 2048;

    /**
     * @param services
     * @param config
     * @return
     */
    @NonNull public static synchronized ClientManager newInstance(ClientConfig config) {
        if (null == m_clientManager) {
            m_clientManager = new ClientManager(config);
        }

        return m_clientManager;
    }

    /**
     * @return
     */
    @Nullable public static synchronized ClientManager get() {
        return m_clientManager;
    }

    /**
     * Initializes a new {@link ClientManager}.
     */
    private ClientManager(@NonNull final ClientConfig clientConfig) {
        super(clientConfig.PDFEXTRACTOR_WORKDIR);

        m_clientConfig = clientConfig;

        m_asyncExecutor = new AsyncExecutor(this, this, ASYNC_CONVERTER_THREADCOUNT, ASYNC_QUEUE_LENTGH);
        m_remoteValidator = new RemoteValidator(m_clientMetrics, clientConfig.REMOTEURL_DOCUMENTCONVERTER);

        try {
            m_pdfExtractor = new PDFExtractor(this, clientConfig.PDFEXTRACTOR_EXECUTABLE_PATH, clientConfig.PDFEXTRACTOR_MAX_VMEM_MB) {
                @Override
                public String getLogPrefix() {
                    return DC_CLIENT_PDF_EXTRACTOR_LOG_PREFIX;
                }
            };
        } catch (@SuppressWarnings("unused") IOException e) {
            logError("DC client PDFExtractor could not be initiaized correctly: " + clientConfig.PDFEXTRACTOR_EXECUTABLE_PATH);
        }

        // check for invalid remote URL
        if ((null == clientConfig.REMOTEURL_DOCUMENTCONVERTER) || (clientConfig.REMOTEURL_DOCUMENTCONVERTER.toString().length() <= 0)) {
            logError("DC client remoteDocumentConverterUrl property is empty or not set at all. Client is not able to perform conversions until a valid Url is set!");
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.config.Reloadable#reloadConfiguration(com.openexchange.config.ConfigurationService)
     */
    @Override
    public void reloadConfiguration(ConfigurationService configService) {
        if (null != m_clientConfig) {
            m_clientConfig.reloadConfiguration(configService);

            m_remoteValidator.updateRemoteURL(m_clientConfig.REMOTEURL_DOCUMENTCONVERTER);

            // check for invalid remote URL
            if ((null == m_clientConfig.REMOTEURL_DOCUMENTCONVERTER) || (m_clientConfig.REMOTEURL_DOCUMENTCONVERTER.toString().length() <= 0)) {
                logError("DC client remoteDocumentConverterUrl property is empty or not set at all. Client is not able to perform conversions until a valid Url is set!");
            }
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.config.Reloadable#getInterests()
     */
    @Override
    public Interests getInterests() {
        return (null != m_clientConfig) ?
            m_clientConfig.getInterests() :
                null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IClientManager#convert(java.lang.String, java.util.HashMap, java.util.HashMap)
     */
    @Override
    public InputStream convert(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        return (new ManagedClientHashJob(this, m_clientConfig.REMOTEURL_DOCUMENTCONVERTER)).process(jobType, jobProperties, resultProperties);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IClientManager#beginPageConversion(java.lang.String, java.util.HashMap, java.util.HashMap)
     */
    @Override
    @Deprecated
    public String beginPageConversion(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        DocumentConverterManager.logError("ManagedClientJob#beginPageConversion is deprecated and must not be called anymore! Use #convert to PDF and PDFExtractor instead.");
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IClientManager#getConversionPage(java.lang.String, int, java.util.HashMap, java.util.HashMap)
     */
    @Override
    @Deprecated
    public InputStream getConversionPage(String jobId, int pageNumber, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        DocumentConverterManager.logError("ManagedClientJob#getConversionPage is deprecated and must not be called anymore! Use #convert to PDF and PDFExtractor instead.");
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IClientManager#endPageConversion(java.lang.String, java.util.HashMap, java.util.HashMap)
     */
    @Override
    @Deprecated
    public void endPageConversion(String jobId, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        DocumentConverterManager.logError("ManagedClientJob#endPageConversion is deprecated and must not be called anymore! Use #convert to PDF and PDFExtractor instead.");
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     */
    public int getRemoteAPIVersion(final boolean force) {
        return (null != m_remoteValidator) ?
            m_remoteValidator.getRemoteAPIVersion(force) :
                0;
    }

    /**
     * Resetting the current remote status.
     * This method should be called, whenever
     * there is an error with the current connection,
     * so that we know, that there's no current
     * connection available.
     */
    @Override
    public void resetRemoteConnection() {
        if ((null != m_remoteValidator) && m_remoteValidator.isRemoteServerSupported()) {
            // reset the status flag at validator
            m_remoteValidator.resetConnection();
        }
    }

    /**
     * @param jobType
     * @param jobProperties
     * @param resultProperties
     */
    public void triggerAsyncConvert(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        m_asyncExecutor.triggerExecution(jobType, jobProperties, resultProperties);
    }

    // - Config ----------------------------------------------------------------

    /**
     * @return
     */
    public DocumentConverterClientMetrics getDocumentConverterClientMetrics() {
        return m_clientMetrics;
    }

    /**
     * @return
     */
    public ClientConfig getClientConfig() {
        return m_clientConfig;
    }

    // - PDFExtractor ----------------------------------------------------------

    @Nullable public PDFExtractor getPDFExtractor() {
        return m_pdfExtractor;
    }

    // - Statics ---------------------------------------------------------------

    final private static int ASYNC_CONVERTER_THREADCOUNT = 3;

    private static ClientManager m_clientManager = null;

    // - ClientManager ---------------------------------------------------------

    final private DocumentConverterClientMetrics m_clientMetrics = new DocumentConverterClientMetrics();

    final private ClientConfig m_clientConfig;

    final private RemoteValidator m_remoteValidator;

    final private AsyncExecutor m_asyncExecutor;

    private PDFExtractor m_pdfExtractor = null;
}
