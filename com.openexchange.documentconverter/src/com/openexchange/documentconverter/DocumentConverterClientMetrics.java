/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import com.openexchange.metrics.micrometer.Micrometer;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;

/**
 * {@link DocumentConverterMetrics}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class DocumentConverterClientMetrics {

    final public static String CONNECTION_STATUS_OK = "ok";
    final public static String CONNECTION_STATUS_UNKNOWNERROR = "unknownerror";

    /**
     * {@link ConnectionStatus}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v8.16.0
     */
    public enum ConnectionType {
        VALIDATION,
        CONVERT
    }

    private static final String GROUP = "appsuite.documentconverterclient";
    private static final String CONNECTIONS = ".connections";
    private static final String VALIDATION_COUNT = ".validation.count";
    private static final String CONVERT_COUNT = ".convert.count";
    private static final String NO_UNIT = null;

    private static final Tags TAGS_CONNECTION_OK = Tags.of("status", "ok");
    private static final Tags TAGS_CONNECTION_UNKNOWN_ERROR = Tags.of("status", "unknownerror");

    // -------------------------------------------------------------------------

    /**
     * Initializes a new {@link DocumentConverterClientMetrics}.
     */
    public DocumentConverterClientMetrics() {
        super();
    }

    // - Public API ------------------------------------------------------------

    /**
     *
     */
    public void incrementConnectionStatusCount(@NonNull final ConnectionType connectionType, @Nullable final Exception e) {
        String connectionStatus = CONNECTION_STATUS_OK;

        if (null != e) {
            final String className = e.getClass().getName();
            final int lastDotPos = className.lastIndexOf(".");

            connectionStatus = (-1 == lastDotPos) ? className : className.substring(lastDotPos + 1);
        }

        final var isValidation = ConnectionType.VALIDATION == connectionType;
        final var metricSuffix = isValidation ? VALIDATION_COUNT : CONVERT_COUNT;
        final var countMapToUse = isValidation ? m_validationCounts : m_convertCounts;
        final var curTags = CONNECTION_STATUS_OK.equals(connectionStatus) ? TAGS_CONNECTION_OK : Tags.of("status", connectionStatus);

        synchronized (countMapToUse) {
            AtomicLong curCount = countMapToUse.get(curTags);

            if (null == curCount) {
                countMapToUse.put(curTags, curCount = new AtomicLong(0));

                Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + CONNECTIONS + metricSuffix, curTags,
                    "The number of established or lost connections to DocumentConverter server.", NO_UNIT, this,
                    (m) -> countMapToUse.get(curTags).get());
            }

            curCount.incrementAndGet();
        }
    }

    // - Members ----------------------------------------------------------------

    final private Map<Tags, AtomicLong> m_validationCounts = new HashMap<>();

    final private Map<Tags, AtomicLong> m_convertCounts = new HashMap<>();
}
